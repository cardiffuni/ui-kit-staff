declare var yam: any;

declare namespace UIKit {
  namespace Module {
    interface ModalBackdrop {
      showBackdrop(): void;
      createBackdrop(): void;
      hideBackdrop(): void;
    }
    interface NavGlobal {
      init(): void;
    }
    interface Carousel {
      init(): void;
      checkNavigation(gallery: HTMLElement): void;
      computeOffset(gallery: HTMLElement): void;
    }
    interface LazyLoader {
      init(): void;
    }
    interface Map {
      init(): void;
      getParameterByName(name: string, querystring: string): string;
    }
    interface Accordion {
      init(): void;
      i18n(key: string, contextOverride?: string): string;
      create(): void;
      openAll(target: JQuery<HTMLElement>, parentID: string): void;
      closeAll(target: JQuery<HTMLElement>, parentID: string): void;
      updateExpanded(button: JQuery<HTMLElement>, status: boolean): void;
      extractLanguageSubstring(input: string): string;
    }
    interface Pills {
      init(): void;
      openAll(): void;
      open(element: string): void;
    }
    interface TabFocus {
      init(): void;
    }
    interface Embed {
      init(): void;
      cardiffPlayer($this: JQuery<HTMLElement>): void;
      twitter($this: JQuery<HTMLElement>): void;
      xerte($this: JQuery<HTMLElement>): void;
    }
    interface Table {
      init(): void;
    }
    interface SubmitForm {
      init(): void;
    }
    interface CircleNavigation {
      init(): void;
      lightenColour(color: string, percent: number): void;
      drawPieSlice(numSegments: number, k: number, radius: number, sliceColour: string): SVGPathElement;
      Draw(graphName: string, options: UIKit.CircleNavigationOptions, textCutoff: number): void;
    }
    interface WPFeed {
      init(): void;
      buildTeasersmall(wpfeedLink: JQuery<HTMLElement>, JSON: UIKit.WpFeedItem): void;
      buildTeaser(wpfeedLink: JQuery<HTMLElement>, JSON: UIKit.WpFeedItem): void;
      buildTeaservertical(
        wpfeedLink: JQuery<HTMLElement>,
        JSON: UIKit.WpFeedItem,
        dataTitle: string,
        dataTitleIcon: string,
        dataTitleURL: string
      ): void;
      formatDate(date: string): void;
      JSONloader(
        url: string,
        wpfeedLink: JQuery<HTMLElement>,
        dataTemplate: string,
        dataTitle: string,
        dataTitleIcon: string,
        dataTitleURL: string
      ): void;
    }
    interface Process {
      init(): void;
      setHeight(process: JQuery<HTMLElement>, bp: string): void;
    }
    interface AppsToolbar {
      init(): void;
      selectedInit(): void;
      searchInArray(obj: any, list: any): boolean;
      removeItemFromArray(item: any): any;
      fetchApplications(url: string, selectLabel: string): void;
      changingActiveItems(): void;
      selectFacets(): void;
      buildApplicationsList(results: any, categories: any, selectLabel: any): void;
      clearSearch(): void;
      updateSliderIcons(): void;
      selectingItems(): void;
      clearActiveItems(): void;
      updateSelectedItems(): void;
      refreshIcons(): void;
      slickSlider(results: any): void;
    }
    interface SearchForm {
      init(): void;
    }
    interface NavSearch {
      init(): void;
    }
    interface Handbook {
      init(): void;
      searchHandbook(): void;
    }
    interface ModuleSearch {
      init(): void;
      searchModule(type: string): void;
      findModuleSch(): void;
      findModuleIndiv(moduleid: any): void;
      nullToNA(val: number): string;
    }
    interface Facet {
      init(): void;
      moveToNextInput(e: JQuery.KeyUpEvent, $this: JQuery<HTMLInputElement>): void;
      dateRangeInputFieldFilter(e: JQuery.KeyDownEvent): void;
      showMoreFacetItems(e: JQuery.ClickEvent, element: JQuery<HTMLElement>): void;
      closeFacetPopup(e: JQuery.ClickEvent): void;
      facetCancelAction(e: JQuery.ClickEvent): void;
      facetGroupTitleClick(e: JQuery.ClickEvent, element: JQuery<HTMLElement>): void;
      swapChevron(element: JQuery<HTMLElement>): void;
      launchFilterPopupMenu(e: JQuery.ClickEvent): void;
      hiddenItems(element: JQuery<HTMLElement>): void;
      shownItems(element: JQuery<HTMLElement>): void;
    }
    interface Tab {
      init(): void;
      extractLanguageSubstring(inputString: string): void;
    }
    interface YammerEmbed {
      init(): void;
      launchYammer(): void;
    }
    interface Alert {
      init(): void;
      create(): void;
      extractLanguageSubstring(input: string): string;
    }
  }
  interface SquizDescription {
    [index: string]: string;
    en: string;
    cy: string;
  }
  interface TranslationItem {
    [index: string]: string;
  }

  namespace Navigation {
    interface Status {
      session: {
        refresh: boolean;
        disable: boolean;
      };
      transition: {
        menu: boolean;
      };
      direction: {
        back: boolean;
      };
      state: {
        menuOpen: boolean;
        waitingResponse: boolean;
        finishedProcessing: boolean;
      };
    }
    namespace Store {
      interface Store {
        [index: string]: UIKit.Navigation.Store.StoreContext;
      }
      interface StoreContext {
        [index: string]: UIKit.Navigation.Store.Item;
      }
      interface Item {
        assetid: number;
        siblings: UIKit.Navigation.Store.Page[];
        children: UIKit.Navigation.Store.Page[];
        lineage: UIKit.Navigation.Store.Page[];
      }
      interface Page {
        assetid: string;
        name: string;
        url: string;
        // eslint-disable-next-line camelcase
        has_children: boolean;
        active: boolean;
        status: {
          // eslint-disable-next-line camelcase
          under_construction: boolean;
          live: boolean;
        };
        showEditingBrackets: boolean;
        // eslint-disable-next-line camelcase
        status_code: number;
      }
    }
  }
  interface Translations {
    [index: string]: TranslationItem;
    en: TranslationItem;
    cy: TranslationItem;
  }
  interface HTMLElementTable extends JQuery<HTMLElement> {
    tablesorter(): void;
  }
  interface EmbedWindow extends Window {
    twttr?: any;
    embedly?: any;
  }
  interface HTMLSlickElement extends JQuery<HTMLElement> {
    slick(settings: any): void;
  }
  interface HTMLSortableElement extends JQuery<HTMLElement> {
    sortable(settings: any): void;
  }

  interface HoverIntentWindow extends JQuery<HTMLElement> {
    hoverIntent?: any;
  }
  interface CircleNavigationOptions {
    [index: string]: any;
    colours: {
      background: string[];
      foreground: string;
    };
    labels: any[];
    fontSize: string;
  }
  interface WpFeedItem {
    found: number;
    posts: WpFeedPostItem[];
  }
  /* eslint-disable camelcase */
  interface WpFeedPostItem {
    ID: number;
    site_ID: number;
    author: any;
    date: string;
    modified: string;
    title: string;
    URL: string;
    short_URL: string;
    content: string;
    excerpt: string;
    slug: string;
    guid: string;
    status: string;
    sticky: boolean;
    password: string;
    parent: boolean;
    type: string;
    comments_open: boolean;
    pings_open: boolean;
    likes_enabled: boolean;
    sharing_enabled: boolean;
    comment_count: number;
    like_count: number;
    i_like: boolean;
    is_reblogged: boolean;
    is_following: boolean;
    global_ID: string;
    featured_image: string;
    post_thumbnail: any;
    format: string;
    geo: boolean;
    menu_order: number;
    publicize_URLs: any;
    tags: any;
    categories: any;
    attachments: any;
    metadata: any;
    meta: any;
    current_user_can: any;
    capabilities: any;
  }
  /* eslint-enale camelcase */
}
declare var matrixData: any;
// eslint-disable-next-line @typescript-eslint/camelcase
declare var Squiz_Matrix_API: any;
