define(['jquery'], ($: JQueryStatic): UIKit.Module.TabFocus => {
  const url = window.location.href;

  const init = (): void => {
    let anchor = url.substring(url.indexOf('#') + 1);

    /* Check if anchor exists and it is a tab */
    if (anchor !== undefined && anchor.length) {
      const elm = $(`a[href="#${anchor}"]`);
     
      if (elm.data('toggle') === 'tab') {
        elm.tab('show');
      }

      if (elm[0] !== undefined) {
        elm[0].scrollIntoView(true);
      }
    }
  };
  return {
    init
  };
});