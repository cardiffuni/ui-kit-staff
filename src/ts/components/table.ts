define(['jquery'], ($: JQueryStatic): UIKit.Module.Table => {
  const init = (): void => {
    $('table').each(function(): void {
      const table = $(this) as UIKit.HTMLElementTable;
      const isWithinBox = table.parents('.box').length > 0;

      if (!table.hasClass('table')) {
        table.addClass('table');
      }
      if (isWithinBox || (table.attr('data-no-responsive') !== undefined && table.attr('data-no-responsive') === 'true') ) {
      } else {
        if (table.parent('.table-responsive').length === 0) {
          table.wrap('<div class="table-responsive">');
        }

        if (table.hasClass('table-sortable')) {
          table.tablesorter()
        }
      }
    });
  };

  return {
    init
  };
});