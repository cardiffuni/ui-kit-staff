define(['touchPunch', 'slick'], (): UIKit.Module.AppsToolbar => {
  let gscope = $('html').attr('lang') === 'en' ? '1' : '2&context=cy';
  const lang = $('html').attr('lang') || 'en';

  const config: any = {
    maxToolbarApps: 7,
    selected: [],
    facets: [],
    url: `https://search.cardiff.ac.uk/s/search.json?collection=intranet-applications&form=search-en&sort=title&num_ranks=200&gscope1=${gscope}`,
    applicationIcons: '',
    appInialised: false,
  };

  const translations: UIKit.Translations = {
    en: {
      facetAll: 'All',
      facetDisabled: 'Filter by category',
      noResultsFound: 'No results found. Check your search terms or try using the category filters instead.',
    },
    cy: {
      facetAll: 'Pob',
      facetDisabled: 'Hidlo yn ôl categori',
      noResultsFound: 'Heb ddod o hyd i ganlyniadau. Gwiriwch eich termau chwilio neu rhowch gynnig ar ddefnyddio hidlyddion categori.',
    },
  };

  const svg = {
    /* eslint-disable max-len */
    menu:
      '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 20"><title>Menu</title><path class="bar" d="M 28 0 H 0 V 4 H 28 V 4 Z"></path><path class="bar" d="M 28 12 H 0 V 8 H 28 v 4 Z"></path><path class="bar" d="M 28 20 H 0 V 16 H 28 v 4 Z"></path></svg>',
    more:
      '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 23.97"><title>Applications</title><path d="M4,4h9.84v9.84H4V4ZM28,4v9.84H18.19V4H28ZM4,18.16h9.84V28H4V18.16Zm14.16,0H28V28H18.19V18.16Z" transform="translate(-4.03 -4.03)"></path></svg>',
    plus:
      '<svg class="icon" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><title>Open</title><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-727.000000, -388.000000)" fill="#383735"><path d="M736.75,388 L736.75,395.25 L744,395.25 L744,397.75 L736.75,397.75 L736.75,405 L734.25,405 L734.25,397.749 L727,397.75 L727,395.25 L734.25,395.249 L734.25,388 L736.75,388 Z"></path></g></g></svg>',
  };

  /**
   * Adds all click event handlers to respective DOM elements.
   */
  const init = (): void => {
    if (matrixData && matrixData.selected) {
      config.selected = matrixData.selected;

      for (let i = config.selected.length - 1; i > 0; i--) {
        if (config.selected[i].isEmpty) {
          config.selected.pop();
        }
      }
    }

    addToolboxClickEvents();

    $('.js-apps-toolbar-toggle').on('click', function (e): void {
      e.preventDefault();
      toggleToolbar();
    });

    $('.js-save-toolbar').on('click', function (): void {
      updateUserConfig();
      $('#search').val('');
      fetchApplications(config.url, '');
    });
  };

  /**
   * Refreshes all click event listeners for all js-apps-toolbar-init items on the page.
   * Responsible for initialising the application.
   */
  const addToolboxClickEvents = (): void => {
    $('.js-apps-toolbar-init').off('click');

    $('.js-apps-toolbar-init').on('click', function (e: JQuery.ClickEvent): void {
      e.preventDefault();

      if (!config.appInialised) {
        config.applicationIcons = matrixData.application_icons;

        const search = $('.apps-toolbar-form');

        search.on('submit', function (e): void {
          e.preventDefault();
          const value = $(this).find('input').val();
          fetchApplications(`${config.url}&query=${value}`, '');
        });

        $('.clear').click(function (): void {
          clearSearch();
        });

        selectedInit();
        fetchApplications(config.url, '');
        setTimeout(refreshIcons, 500);
        toggleToolbar();
        config.appInialised = true;

      } else {
        toggleToolbar();
      }
    });
  };

  /**
   * Loads the icons onto the slider if they fail to load in themselves.
   */
  const refreshIcons = (): void => {
    const iconObj = $('body').children('svg');

    if (iconObj.length) {
      const sliderItems = $('.apps-toolbar-slider .apps-toolbar-slider-item');

      sliderItems.each(function (): void {
        const id = $(this).data('id');
        const symbol = $(`symbol#${id}`);
        const contents = symbol.html();
        const viewbox = symbol.attr('viewBox');

        if (viewbox !== undefined) {
          $(this).find('use').replaceWith(`<svg viewbox="${viewbox}" id="${id}">${contents}</svg>`);
        }
      });
    }
  };

  /**
   * Hides or shows the apps-toolbar
   */
  const toggleToolbar = (): void => {
    const toolbar = $('.apps-toolbar');

    if (toolbar.length) {
      if (!$('.apps-toolbar').hasClass('d-none')) {
        toolbar.addClass('d-none');
        $('body').removeClass('overflow-hidden');

      } else {
        toolbar.removeClass('d-none');

        if (config.appInialised === true) {
          const slider = $('.apps-toolbar-slider') as UIKit.HTMLSlickElement;
          slider.slick('setPosition');
        }

        $('body').addClass('overflow-hidden');
      }
    }
  };

  /**
   * Initialises the selected applications at the top of the apps-toolbar
   */
  const selectedInit = (): void => {
    const target = $('.apps-toolbar-selected ul') as UIKit.HTMLSortableElement;
    const selected = config.selected;
    const amount = selected.length > config.maxToolbarApps ? selected.length : config.maxToolbarApps;

    target.empty();

    for (var i = 0; i < amount; i++) {
      if (selected[i]) {
        const selectedItemTemplate = `<li class="apps-toolbar-selected-item" 
          data-value="${selected[i].name}"
          data-id="${selected[i].id}" 
          data-url="${selected[i].url}" 
          data-assetid="${selected[i].assetid}">
          <div class="apps-toolbar-selected-item-dragdrop">${svg['menu']}</div>
          <div class="apps-toolbar-icon">
            <svg class="icon"><use xlink:href="${config.applicationIcons}#${selected[i].id}"></use></svg>
          </div>
          <a class="apps-toolbar-selected-item-remove" 
            tabIndex="0" 
            data-value="${selected[i].name}" 
            data-id="${selected[i].id}"></a>
          <div class="apps-toolbar-selected-item-circle"></div>
          <span class="apps-toolbar-selected-item-text">${selected[i].name}</span>
        </li>`;

        target.append(selectedItemTemplate);
      } else {
        target.append(`<li class="apps-toolbar-selected-item-empty">${svg['plus']}</li>`);
      }
    }

    target.sortable({
      axis: 'y',
      placeholder: 'ui-state-highlight',
      items: '.apps-toolbar-selected-item',
      handle: '.apps-toolbar-selected-item-dragdrop',
      tolerance: 'pointer',
      update: function (): void {
        const items = $('.apps-toolbar-selected-item');
        let newSelectedArray: any = [];

        for (let i = 0; i < items.length; i++) {
          let obj = {};

          obj = {
            name: $(items[i]).data('value'),
            id: $(items[i]).data('id'),
            url: $(items[i]).data('url'),
            assetid: $(items[i]).data('assetid'),
          };

          newSelectedArray.push(obj);
        }

        config.selected = newSelectedArray;
      },
    });
  };

  /**
   * Search for a given object in an array.
   * @param obj The object to search for
   * @param list The array to search in
   */
  const searchInArray = (obj: any, list: any): boolean => {
    for (var i = 0; i < list.length; i++) {
      if (list[i].id === obj.id) {
        return true;
      }
    }
    return false;
  };

  /**
   * Removes an item from the selected items array.
   * @param item The item to remove from the array
   */
  const removeItemFromArray = (item: any): any => {
    let selected: any = config.selected;
    selected = $.grep(selected, function (selectedItem: any): any {
      return selectedItem.id !== item.id;
    });
    return selected;
  };

  /**
   * Fetches the required applications from funnelback
   * @param url The url to get the applications from.
   * @param selectLabel The select option to filter the response with.
   */
  // Fetch applications from funnelback
  const fetchApplications = (url: string, selectLabel: string): void => {
    $.ajax({
      method: 'get',
      url: url,
      success: function (response): void {
        const results = response.response.resultPacket.results;
        if (config.facets.length === 0) {
          config.facets = response.response.facets[0];
        }
        buildApplicationsList(results, config.facets, selectLabel);
        slickSlider(results.length);
        selectingItems();
        changingActiveItems();
      },
    });
  };

  /**
   * Adds click events to remove selected applications.
   */
  const changingActiveItems = (): void => {
    $('.apps-toolbar-selected-item-remove').on('click', function (): void {
      config.selected = $.extend(
        true,
        [],
        removeItemFromArray({
          id: $(this).data('id'),
        })
      );

      const target = $('.apps-toolbar-slider-item.active');

      for (var i = 0; i < target.length; i++) {
        if ($(target[i]).data('value') == $(this).data('value')) {
          $(target[i]).removeClass('active');
        }
      }

      updateSelectedItems();
    });
  };

  /**
   * Configures the select options for the form section.
   */
  const selectFacets = (): void => {
    const select = $('.apps-toolbar select#options');
    const search = $('.apps-toolbar-form');
    const searchInput = $(search).find('input').val();

    select.on('change', function (): void {
      const value = $(this).val();
      const label = $(this).find('option:selected').text();
      const noResultsFound = $('.apps-toolbar-slider').children('p');

      if (searchInput === '' || noResultsFound.length > 0) {
        fetchApplications(`${config.url}&${value}`, label);
        clearSearch();
      } else {
        fetchApplications(`${config.url}&${value}&query=${searchInput}`, label);
      }
    });
  };

  /**
   * Builds the list of applications for the slick slider.
   * @param results The response from the API call.
   * @param categories An array of categories to filter the list with.
   * @param selectLabel The select option to filter the results with.
   */
  const buildApplicationsList = (results: any, categories: any, selectLabel: any): void => {
    const applicationsTarget = $('.apps-toolbar-slider');
    const filtersTarget = $('.apps-toolbar-slider-options');
    const facets = categories.categories[0].values;
    const select = $('<select name="options" id="options"></select>');
    const selectWrapper = $('<div class="apps-toolbar-select-wrapper"></div>');
    selectLabel = selectLabel || '';

    applicationsTarget.html('').removeClass('slick-initialized slick-slider slick-dotted');
    filtersTarget.find('.apps-toolbar-select-wrapper').remove();

    if (results.length != 0) {
      for (var i = 0; i < results.length; i++) {
        const item = results[i];
        const active = searchInArray({ id: item.metaData.appID }, config.selected) ? ' active' : '';

        const sliderItemTemplate = `<div class="apps-toolbar-slider-item ${active}" 
          data-value="${item.title}" 
          data-id="${item.metaData.appID}" 
          data-url="${item.metaData.U}" 
          data-assetid="${item.metaData.assetid}">
          <div class="apps-toolbar-icon">
            <svg class="icon" role="img"><defs><style>.cls-1{fill:#fff;}</style></defs><use xlink:href="${config.applicationIcons}#${item.metaData.appID}"></use></svg>
          </div>
          <p>${item.title}</p>
          <a class="apps-toolbar-slider-item-icon" data-value="${item.title}"></a>
        </div>`;

        applicationsTarget.append(sliderItemTemplate);
      }
    } else {
      applicationsTarget.append(`<p>${translations[lang].noResultsFound}</p>`);
    }

    for (var i = 0; i < facets.length; i++) {
      const selected = selectLabel == facets[i].label ? 'selected' : '';
      if (facets[i].label !== 'All') {
        let option = $(`<option value="${facets[i].queryStringParam}" ${selected}>${facets[i].label}</option>`);
        select.append(option);
      }
    }
    const selected = selectLabel == translations[lang].facetAll ? 'selected' : '';
    let option = $(`<option value="" ${selected}>${translations[lang].facetAll}</option>`);
    select.prepend(option);

    const label = `<label class="sr-only" for="options">${translations[lang].facetDisabled}</label>`;

    if (selectLabel == '') {
      option = $(`<option value="" disabled selected>${translations[lang].facetDisabled}</option>`);
      select.prepend(option);
    }
    selectWrapper.append(label);
    selectWrapper.append(select);
    filtersTarget.prepend(selectWrapper);
    selectFacets();
  };

  /**
   * Helper function to clear the search input.
   */
  const clearSearch = (): void => {
    $('.apps-toolbar-form input').val('');
  };

  /**
   * Updates the icons in the slider to represent the state of the selected applications.
   */
  const updateSliderIcons = (): void => {
    if (config.selected.length === config.maxToolbarApps) {
      $('.apps-toolbar-slider-item:not(.active) .apps-toolbar-slider-item-icon').addClass('disabled');
    } else {
      $('.apps-toolbar-slider-item:not(.active) .apps-toolbar-slider-item-icon').removeClass('disabled');
    }
    updateTabIndex();
  };

  /**
   * Updates the tabIndex of the slider columns and item icons to ensure that the user can only
   * tab through non-disabled buttons in the sldier.
   *
   */
  const updateTabIndex = (): void => {
    $('.slick-slide').each(function (): void {
      $(this).attr('tabindex', '-1');
    });

    const sliderIcons = $('.apps-toolbar-slider-item-icon');
    sliderIcons.each(function (): void {
      if ($(this).hasClass('disabled')) {
        $(this).attr('tabindex', '-1');
      } else {
        $(this).attr('tabindex', '0');
      }
    });
  };

  /**
   * Selecting items for the top panel.
   */
  const selectingItems = (): void => {
    updateSliderIcons();
    $('.apps-toolbar-slider-item-icon').on('click', function (): void {
      const obj = {
        name: $(this).parent().data('value'),
        id: $(this).parent().data('id'),
        url: $(this).parent().data('url'),
        assetid: $(this).parent().data('assetid'),
      };

      var doesExist = searchInArray(obj, config.selected);

      if (config.selected.length < config.maxToolbarApps) {
        if (!$(this).parent().hasClass('active')) {
          if (!doesExist) {
            config.selected.push(obj);
            $(this).parent().addClass('active');
            updateSelectedItems();
          }
        } else {
          if (doesExist) {
            config.selected = $.extend(true, [], removeItemFromArray(obj));
            $(this).parent().removeClass('active');
          }
        }

        updateSelectedItems();
      } else {
        if ($(this).parent().hasClass('active')) {
          if (doesExist) {
            config.selected = $.extend(true, [], removeItemFromArray(obj));
            $(this).parent().removeClass('active');
          }
          updateSelectedItems();
        }
      }
      updateSliderIcons();
    });
  };

  /**
   * Removes event handlers.
   */
  const clearActiveItems = (): void => {
    $('.apps-toolbar-selected-item-remove').off('click');
    $('.apps-toolbar-slider-item-icon').off('click');
    $('.apps-toolbar-slider-item').off('click');
  };

  /**
   * Updating the top panel with selected applications
   */

  const updateSelectedItems = (): void => {
    clearActiveItems();
    selectedInit();
    changingActiveItems();
    selectingItems();
    updateTabIndex();
  };

  /**
   * Slick slider initialization for bottom panel with applications
   * @param results The results from the API call.
   */
  const slickSlider = (results: any): void => {
    $(window).off('resize');

    const slickSettings = {
      slidesToShow: 6,
      slidesToScroll: 6,
      accessibility: true,
      rows: 3,
      dots: true,
      arrows: false,
      infinite: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            rows: 3,
          },
        },
      ],
    };

    const windowWidthInit = $(window).width() || 0;
    const slider = $('.apps-toolbar-slider') as UIKit.HTMLSlickElement;

    if (windowWidthInit >= 480 && results > 18) {
      slider.slick(slickSettings);
    }

    $(window).on('resize', function (): void {
      const windowWidthTemp = $(window).width() || 0;

      if (windowWidthTemp < 480) {
        if (slider.hasClass('slick-initialized')) {
          slider.slick('unslick');
        }
      } else {
        if (slider.hasClass('slick-initialized')) {
          if (results > 18) {
            slider.slick('unslick');
            slider.slick(slickSettings);
          } else {
            slider.slick('unslick');
          }
        } else {
          if (results > 18) {
            slider.slick(slickSettings);
          }
        }
      }
      updateSelectedItems();
    });
  };

  /**
   * Updates the users applications.
   */
  /* eslint-disable @typescript-eslint/camelcase */
  const updateUserConfig = (): void => {
    let jsApiOptions: any = [];
    let api: any = null;
    const selected = JSON.parse(JSON.stringify(config.selected));
    const selectedLen = selected.length;
    jsApiOptions.key = matrixData.jsApiKey;

    for (let i = 0; i < selectedLen; i++) {
      selected[i].name = `%globals_asset_attribute_name:${selected[i].assetid}%`;
      selected[i].url = `%globals_asset_metadata_application.url:${selected[i].assetid}%`;
    }

    // eslint-disable-next-line
    if (typeof Squiz_Matrix_API !== 'undefined') {
      api = new Squiz_Matrix_API(jsApiOptions);
    }

    if (api) {
      /* eslint-disable camelcase */
      api.setMetadata({
        asset_id: matrixData.user,
        field_id: '1219830',
        field_val: JSON.stringify(selected),
        dataCallback: function (response: any): void {
          if (response.hasOwnProperty('error')) {
            api.acquireLock({
              asset_id: matrixData.user,
              screen_name: 'metadata',
              dependants_only: 0,
              force_acquire: 1,
              dataCallback: function (): void {
                setTimeout(function (): void {
                  api.contextualiseAsset({
                    asset_id: matrixData.user,
                    context_id: '1',
                    contextualise: true,
                    component_type: 'metadata',
                    dataCallback: function (): void {
                      api.setMetadata({
                        asset_id: matrixData.user,
                        field_id: '1219830',
                        field_val: JSON.stringify(selected),
                        dataCallback: function (response: any): any {
                          if (response.hasOwnProperty('error')) {
                            // eslint-disable-next-line
                            console.log(`error: ${response.error}`);
                            return false;
                          } else {
                            updateToolbox();
                          }
                        },
                      });
                    },
                  });
                }, 1000);
              },
            });
          } else {
            updateToolbox();
          }
        },
      });

    } else {
      // eslint-disable-next-line
      console.log('Settings save failed');
    }
  };
  /* eslint-enable @typescript-eslint/camelcase */

  /**
   * Updates the toolbox on the homepage.
   */
  const updateToolbox = (): void => {
    const oldToolboxApplications = $('.toolbox-item');
    let toolboxIconsOutput = '';
    const selectedLen = config.selected.length;
    for (let i = 0; i < selectedLen; i++) {
      toolboxIconsOutput += `<li class="toolbox-item">
        <a href="${config.selected[i].url}" class="ga-event" 
          data-action="click" 
          data-category="Intranet toolbox - staff" 
          data-label="${config.selected[i].name}" 
          target="_blank">
          <svg class="icon" role="img"><defs><style>.cls-1{fill:#fff;}</style></defs><use xlink:href="${config.applicationIcons}#${config.selected[i].id}"></use></svg>
          ${config.selected[i].name}
        </a>
      </li>`;
    }

    const missingApps = config.maxToolbarApps - config.selected.length;

    if (missingApps > 0) {
      for (let i = 0; i < missingApps; i++) {
        toolboxIconsOutput += `<li class="toolbox-item empty">
          <a class="js-apps-toolbar-init" tabindex="0">
            ${svg['plus']}Add tool
          </a>
        </li>`;
      }
    }
    toolboxIconsOutput += `<li class="toolbox-item">
      <a href="https://intranet.cardiff.ac.uk/staff/applications">
        ${svg['more']}More online tools
      </a>
    </li>`;

    if (oldToolboxApplications.length) {
      oldToolboxApplications.last().after(toolboxIconsOutput);
      oldToolboxApplications.remove();
    } else {
      $('.toolbox-links').prepend(toolboxIconsOutput);
    }

    toggleToolbar();
    addToolboxClickEvents();
  };

  return {
    init,
    selectedInit,
    searchInArray,
    removeItemFromArray,
    fetchApplications,
    changingActiveItems,
    selectFacets,
    buildApplicationsList,
    clearSearch,
    updateSliderIcons,
    selectingItems,
    clearActiveItems,
    updateSelectedItems,
    refreshIcons,
    slickSlider,
  };
});
