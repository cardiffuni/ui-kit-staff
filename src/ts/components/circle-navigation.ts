define(['jquery'], ($: JQueryStatic): UIKit.Module.CircleNavigation => {
  /**
   * A function that to return a segment of a circle navigation object.
   * @param numSegments The total number of segments .
   * @param segmentNumber The current segment.
   * @param radius The radius of the circle navigation.
   * @param sliceColour The colour of the segment.
   */
  const drawPieSlice = (numSegments: number, segmentNumber: number, radius: number, sliceColour: string): SVGPathElement => {
    // Calculating the coordinates of point 'A'
    let startPointX = radius + radius * Math.sin((2 * Math.PI * (segmentNumber - 1)) / numSegments);
    let startPointY = radius - radius * Math.cos((2 * Math.PI * (segmentNumber - 1)) / numSegments);

    // Calculating the coordinates of point 'B'
    let endPointX = radius + radius * Math.sin((2 * Math.PI * segmentNumber) / numSegments);
    let endPointY = radius - radius * Math.cos((2 * Math.PI * segmentNumber) / numSegments);

    // Defining the path data. See documentation: https://www.w3schools.com/graphics/svg_path.asp
    let pathData = [`M${startPointX} ${startPointY}`, `A${radius} ${radius} 0 0 1 ${endPointX} ${endPointY}`, `L${radius} ${radius}`, `Z`].join(' ');

    let segment = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    segment.setAttribute('d', pathData);
    segment.setAttribute('fill', sliceColour);

    return segment;
  };

  /**
   * A function to draw a circle navigation object in the DOM.
   * @param graphName The id of the circle navigation.
   * @param options A JSON object that controls the configuration of the circle navigation.
   * @param textCutoff The length of the first line of text for each segment, in number of characters.
   */
  const Draw = (graphName: string, options: UIKit.CircleNavigationOptions, textCutoff: number): void => {
    $('body').append('<span id=\'ruler\' style=\'visibility:hidden; white-space:nowrap\'></span>');

    // Getting the width of the Circle Navigation object and defining the radius.
    let radius = 0;
    const width = $(`#${graphName}`).width();
    if (width !== undefined) {
      radius = width / 2;
    }

    //The inside angle of each segment. All segments are of equal size
    let segmentArcRadian: number;

    //The midpoint of the inside angle. Used when placing the text in the center of each segment
    let thRadian: number;

    // The radius of each segment, used when calculating the coordinates of the center of each segment
    const segmentRadius = (radius / 3) * 2;

    // The total number of segments in the Circle Navigation Object.
    const numSegments: number = options.labels.length;

    segmentArcRadian = (360 / numSegments) * (Math.PI / 180);
    thRadian = segmentArcRadian / 2;

    // Looping through to begin drawing each segment
    for (var k = 1; k < numSegments + 1; k++) {
      let text = options['labels'][k - 1][0];

      for (var j = 0; j < text.length; j++) {
        if (text[j + 1] && text[j].length + text[j + 1].length < textCutoff) {
          options.labels[k - 1][0][j] = text[j] + ' ' + text[j + 1];
          options.labels[k - 1][0].splice(j + 1, 1);
        }
      }

      let segment: SVGPathElement = drawPieSlice(numSegments, k, radius, options.colours['background'][k - 1]);

      // Calculate the coordinates of the midpoint of the segment
      let segmentX = 0;
      let segmentY = 0;

      if (radius !== undefined) {
        segmentX = radius + segmentRadius * Math.sin(thRadian);
        segmentY = radius - segmentRadius * Math.cos(thRadian);
      }

      segmentY = segmentY - 5 * text.length;

      // Create the anchor element for the segment
      let anchorElement: SVGAElement = document.createElementNS('http://www.w3.org/2000/svg', 'a');
      anchorElement.setAttribute('href', options['labels'][k - 1][1]);
      $(`#${graphName}`).append(anchorElement);
      $(anchorElement).append(segment);

      // Create the text element for the segment
      let textElement: SVGTextElement = document.createElementNS('http://www.w3.org/2000/svg', 'text');
      textElement.setAttribute('fill', options.colours['foreground']);
      textElement.setAttribute('font-size', options['fontSize']);
      $(anchorElement).append(textElement);

      for (var i = 0; i < text.length; i++) {
        $('#ruler').text(text[i]);

        // Create the tSpanElement elements for the text element
        let tSpanElement: SVGTSpanElement = document.createElementNS('http://www.w3.org/2000/svg', 'tspan');
        tSpanElement.setAttribute('x', ((segmentX || 0) - ($('#ruler').width() || 0) / 2).toString());
        tSpanElement.setAttribute('y', segmentY.toString());
        $(textElement).append(tSpanElement);
        $(tSpanElement).text(text[i]);
        segmentY = segmentY + 18;
      }
      thRadian = thRadian + segmentArcRadian;
    }

    // Create the middle circle part of the circle navigation
    let circleElement = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    circleElement.setAttribute('cx', radius.toString());
    circleElement.setAttribute('cy', radius.toString());
    circleElement.setAttribute('r', ((radius || 0) / 2.5).toString());
    circleElement.setAttribute('fill', 'white');

    $(`#${graphName}`).append(circleElement);
    $('#ruler').remove();

    $(`svg#${graphName} a`).each(function(index): void {
      $(this)
        .children()
        .each(function(): void {
          $(this).click(function(): void {
            if (options['labels'][index][1].length) {
              window.location.href = options['labels'][index][1];
            }
          });
        });
    });
  };

  /**
   * Initialises all circle navigation objects on the page
   */
  const init = (): void => {
    $(document).ready(function(): void {
      $('ol.circle-navigation').each(function(): void {
        const textCutoff = 14;

        // Begin retrieving various data from the circle navigation list.
        const graphName = $(this).attr('data-graphName') || '';
        const graphBaseBgColour = $(this).attr('data-graphBaseBgColour') || '';
        const colourFg = $(this).attr('data-graphFgColour') || '';
        const fontSize = $(this).attr('data-labelFontSize') || '';
        const parentWidth =
          $(this)
            .parent()
            .width() || 0;

        let options: UIKit.CircleNavigationOptions = {
          colours: {
            background: [],
            foreground: colourFg
          },
          labels: [],
          fontSize: fontSize
        };

        $(this)
          .children()
          .each(function(index): void {
            let text = `${index + 1}. ${$(this)
              .children('a')
              .text()}`;
            let textArray: string[] = text.split(' ');
            textArray[0] = textArray[0] + ' ' + textArray[1];
            textArray.splice(1, 1);
            options.labels.push([
              textArray,
              $(this)
                .children('a')
                .attr('href')
            ]);
          });

        // Create the circle navigation SVG
        let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        svg.setAttribute('id', graphName);
        svg.setAttribute('height', parentWidth.toString());
        svg.setAttribute('width', parentWidth.toString());

        $(`.circle-navigation[data-graphName="${graphName}"]`).after(svg);
        $(`.circle-navigation[data-graphName="${graphName}"]`).remove();

        let percent = 0;
        let percentLightest = 26;
        let percentIncrement = percentLightest / (options.labels.length - 1);

        for (let i = 0; i < options.labels.length; i++) {
          options.colours.background.push(lightenColour(graphBaseBgColour, percent));
          percent += percentIncrement;
        }
        Draw(graphName, options, textCutoff);
      });
    });
  };

  /**
   * Lightens a colour.
   * @param color The colour to lighten, in hexidecimal format.
   * @param percent The percentage to lighten the colour by.
   * @see: https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
   */
  const lightenColour = (color: string, percent: number): string => {
    let colourHex = parseInt(color.slice(1), 16);
    let amount = Math.round(2.55 * percent);
    let red = (colourHex >> 16) + amount;
    let green = ((colourHex >> 8) & 0x00ff) + amount;
    let blue = (colourHex & 0x0000ff) + amount;

    return (
      '#' +
      (
        0x1000000 +
        (red < 255 ? (red < 1 ? 0 : red) : 255) * 0x10000 +
        (green < 255 ? (green < 1 ? 0 : green) : 255) * 0x100 +
        (blue < 255 ? (blue < 1 ? 0 : blue) : 255)
      )
        .toString(16)
        .slice(1)
    );
  };

  return {
    init,
    drawPieSlice,
    Draw,
    lightenColour
  };
});
