define(['jquery'], ($: JQueryStatic): UIKit.Module.YammerEmbed => {
  const config = {
    cutoff: parseInt($('#yammer-login').attr('data-charLimit') || '20'),
    yammerScriptSrc: 'https://c64.assets-yammer.com/assets/platform_js_sdk.js',
    yammerAppID: 'pTE2y5ttHN5yuUmwtHh8rQ',
    groupURLStart:
      'https://www.yammer.com/cardiff.ac.uk/#/threads/inGroup?type=in_group&feedId='
  }

  const init = (): void => {
    let script = config.yammerScriptSrc;
    let yammerScript = document.createElement('script');
    yammerScript.type = 'text/javascript';
    yammerScript.setAttribute(
      'data-app-id',
      config.yammerAppID
    );
    yammerScript.src = script;
    document.head.appendChild(yammerScript);

    yammerScript.onload = function (): void {
      launchYammer();
    };
  }

  const launchYammer = (): void => {
    yam.connect.loginButton('#yammer-login', function (resp: any): void {
      if (resp.authResponse) {
        yam.getLoginStatus(function (response: any): void {
          if (response.authResponse) {
            const yammerLimit = $('#yammer-login').attr('data-msgLimit');

            yam.platform.request({
              url:`messages/in_group/${$('#yammer-login').attr('data-groupID')}.json?threaded=true`,
              method: 'GET',
              data: {
                limit:
                  yammerLimit === undefined || yammerLimit.length === 0
                    ? 20
                    : $('#yammer-login').attr('data-msgLimit')
              },

              beforeSend(): void {
                $('#yammerSignIn').after('<div class=\'well\' id=\'YamLoading\'><p>Loading...</p></div>');
                $('#yammerSignIn').hide();
              },
              success(data: any): void {
                let type = $('#yammer-login').attr('data-yamType');
                let html = '';
                let groupID = $('#yammer-login').attr('data-groupID');
                let feedHeader = $('#yammer-login').attr('data-Header');

                if (type === 'news-compact') {
                  html += '<ul class=\'media-list-small\'>';
                } else if (type === 'compact') {
                  if (feedHeader !== undefined && feedHeader.length === 0 || feedHeader === ' ') {
                    html += `<section class='box box-related' id='posts'>
                              <div class='box-body'>
                                <ul class='media-list-small'>`;
                  } else {
                    html += `<section class='box box-related' id='posts'>
                              <header class='box-header'> <h1 class='box-title'>${feedHeader}</h1></header>
                              <div class='box-body'>
                                <ul class='media-list-small'>`;
                  }
                } else {
                  if (feedHeader !== undefined && feedHeader.length === 0 || feedHeader === ' ') {
                    html += '<section id=\'posts\'><ul class=\'media-list\'>';
                  } else {
                    html += `<section id='posts'><h2>${feedHeader}</h2><ul class='media-list'>`;
                  }
                }
                for (let i = 0; i < data['messages'].length; i++) {

                  let senderID = data['messages'][i]['sender_id']; 
                  let senderName;
                  let profileURL;
                  let senderPhoto;

                  for (let l = 0; l < data['references'].length; l++) {
                    if (senderID === data['references'][l]['id']) {
                      senderName = data['references'][l]['full_name'];
                      senderPhoto = data['references'][l]['mugshot_url'];
                      profileURL = data['references'][l]['web_url'];
                    }
                  }

                  let messageCreated = data['messages'][i]['created_at'].replace('+0000', '');
                  let dateTime = messageCreated.split(' ');
                  messageCreated = dateTime[0].replace(/\//g, '-') + 'T' + dateTime[1] + 'Z';
                  messageCreated = timeDifference(new Date(), new Date(messageCreated));

                  let messageBody = data['messages'][i]['body']['plain']; 

                  let imageURL = '';

                  let postURL = data['messages'][i]['web_url'];

                  if (type === 'compact' || type === 'news-compact') {

                    if (data['messages'][i]['attachments'].length > 0) {
                      if (data['messages'][i]['attachments'][0]['type'] === 'message' &&
                        data['messages'][i]['attachments'][0]['body']['plain'].length > 0) {
                        let quote = data['messages'][i]['attachments'][0]['body']['plain']; 

                        html += `<li class='media' style='word-break:break-word'>
                          <div class='media-body'>
                            <a href='${postURL}' target='_blank' rel='noopener noreferrer'>`;

                        if (messageBody.length + quote.length > config.cutoff) {
                          if (messageBody.length > config.cutoff) {
                            let trimmedString = messageBody.substring(0, config.cutoff);
                            let trimmedMessage = trimmedString.substr(0,Math.min(trimmedString.length, trimmedString.lastIndexOf(' ')));
                            html += `<p>${trimmedMessage}...</p>`;
                          } else {
                            let messageLen = messageBody.length;
                            let newQuoteLen = config.cutoff - messageLen;
                            let trimmedString = quote.substring(0, newQuoteLen);
                            let trimmedQuote = trimmedString.substr(0, Math.min(trimmedString.length,trimmedString.lastIndexOf(' ')));
                            if (messageBody.length > 0) {
                              html += `<p>${messageBody}</p>`;
                            }
                            if (trimmedQuote.length > 0) {
                              html += `<p class='media-quote'>"${trimmedQuote}...</p>`;
                            }
                          }

                          html += `</a>
                            <div class='media-author'>Posted by ${senderName} | 
                              <time class='date media-meta-date' datetime='${dateTime[0].replace(/\//g, '-')} ${dateTime[1]}'>${messageCreated}</time>
                            </div>
                          </div></li>`;
                        } else {
                          html += `<p>${messageBody}</p>
                            <p class='media-quote'>${quote}</p>
                            </a>
                            <div class='media-author'
                              Posted by ${senderName} | 
                              <time class='date media-meta-date' datetime='${dateTime[0].replace(/\//g, '-')} ${dateTime[1]}'>
                                ${messageCreated}
                              </time>
                            </div>
                            </div></li>`;
                        }
                      } else if (messageBody.length > 0) {
                        html += printDefaultCompactPost(senderName, messageCreated, postURL, messageBody, dateTime);
                      }
                    } else if (messageBody.length > 0) {
                      html += printDefaultCompactPost(senderName, messageCreated, postURL, messageBody, dateTime);
                    }
                  } else {
                    if (data['messages'][i]['attachments'].length > 0) {
                      if ( data['messages'][i]['attachments'][0]['type'] === 'message' &&
                        data['messages'][i]['attachments'][0]['body']['plain'].length > 0 ) {
                        if (messageBody.length > 0) {
                          html += `<li class='media'>
                            <img class='media-object float-left rounded-circle' src='${senderPhoto}' width='70px' height='70px'>
                            <div class='media-body'>
                              <p>${messageBody}</p>
                              <p class='media-quote'>${data['messages'][i]['attachments'][0]['body']['plain']}</p>
                              <div class='media-author'>
                                Posted by <a href='${profileURL}' target='_blank' rel='noopener noreferrer'>
                                  ${senderName}</a> | 
                                  <time class='date media-meta-date' datetime='${dateTime[0].replace(/\//g, '-')} ${dateTime[1]}'>
                                    ${messageCreated}
                                  </time>
                            </div>
                            <div class='media-reply'>
                              <a href='${postURL}' target='_blank' rel='noopener noreferrer'>View Yammer post</a>
                            </div>
                          </div>
                        </li>`;
                        } else {
                          html += `<li class='media'>
                            <img class='media-object float-left rounded-circle' src='${senderPhoto}' width='70px' height='70px'>
                            <div class='media-body'>
                              <p class='media-quote'>${data['messages'][i]['attachments'][0]['body']['plain']}</p>
                              <div class='media-author'>
                                Posted by <a href='${profileURL}' target='_blank' rel='noopener noreferrer'>${senderName}</a> | 
                                <time class='date media-meta-date' datetime='${dateTime[0].replace(/\//g, '-')} ${dateTime[1]}'>
                                  ${messageCreated}
                                </time>
                            </div>
                            <div class='media-reply'>
                              <a href='${postURL}' target='_blank' rel='noopener noreferrer'>View Yammer post</a>
                            </div>
                          </div>
                          </li>`
                        }
                      } else if (data['messages'][i]['attachments'][0]['type'] === 'image') {
                        imageURL = data['messages'][i]['attachments'][0]['image']['url'];
                        html += `<li class='media'>
                          <img class='media-object float-left rounded-circle' src='${senderPhoto}' width='70px' height='70px'>
                          <div class='media-body'>
                            <p>${messageBody}</p>
                            <img class='media-object' style='margin-bottom:20px' src='${imageURL}'>
                            <div class='media-author'>Posted by <a href='${profileURL}' target='_blank' rel='noopener noreferrer'>
                              ${senderName}</a> | 
                              <time class='date media-meta-date' datetime='${dateTime[0].replace(/\//g, '-')} ${dateTime[1]}'>${messageCreated}</time>
                            </div>
                            <div class='media-reply'>
                              <a href='${postURL}' target='_blank' rel='noopener noreferrer'>View Yammer post</a>
                            </div>
                          </div>
                        </li>`;
                      } else if (messageBody.length > 0) {
                        html += printDefaultPost(senderPhoto, senderName, messageCreated, messageBody, postURL, dateTime, profileURL);
                      }
                    } else if (messageBody.length > 0) {
                      html += printDefaultPost(senderPhoto, senderName, messageCreated, messageBody, postURL, dateTime, profileURL);
                    }
                  }
                }

                html += '</ul>';

                if (type === 'compact') {
                  html += `</div><div class='box-footer'>
                    <p><a href='${config.groupURLStart}${groupID}' target='_blank' rel='noopener noreferrer'>
                      See more from ${data['meta']['feed_name']} on Yammer
                    </a></p>`;

                  html += `</div></section></section>`;

                } else if (type !== 'news-compact') {
                  html += `<p class='float-right'>
                      <a class='btn-link btn-lg' href='${config.groupURLStart}${groupID}' target='_blank' rel='noopener noreferrer'>
                        See more from ${data['meta']['feed_name']} on Yammer
                      </a>
                    </p></section>`;
                }

                $('#YamLoading').after(html);
                $('#YamLoading').remove();
                $('#yammerSignIn').remove();
              },
              error(): void {
                // eslint-disable-next-line no-console
                console.log(
                  'Unable to perform request. TIP - Ensure that you are viewing this page over a secure connection.'
                );
              }
            });
          } else {
            // eslint-disable-next-line no-console
            console.log('Unable to login; authentication failed.');
          }
        });
      } else {
        // eslint-disable-next-line no-console
        console.log(
          'An error has occured. TIP - Ensure that you are viewing this page over a secure connection.'
        );
      }
    });

    const printDefaultCompactPost =(senderName: string, messageCreated: string, postURL: string, messageBody: string, dateTime: string[]): string => {
      let html = `<li class='media' style='word-break:break-word'>
        <div class='media-body'>
          <p><a href='${postURL}' target='_blank' rel='noopener noreferrer'>`;

      if (messageBody.length > config.cutoff) {
        let trimmedString = messageBody.substring(0, config.cutoff);
        html += trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(' '))) + '...';
      } else {
        html += messageBody;
      }

      html += `</a></p>
          <div class='media-author'>
            Posted by ${senderName} |
            <time class='date media-meta-date' datetime='${dateTime[0].replace(/\//g, '-')} ${dateTime[1]}'> ${messageCreated}</time>
          </div>
        </div></li>`;
          
      return html;
    }

    const printDefaultPost = (
      senderPhoto: string,
      senderName: string,
      messageCreated: string,
      messageBody: string,
      postURL: string,
      dateTime: string,
      profileURL: string ): string => {
 
      let html = `<li class='media'>
        <img class='media-object float-left rounded-circle' src='${senderPhoto}' width='70px' height='70px'>
        <div class='media-body'>
          <p>${messageBody}</p>
          <div class='media-author'>
            Posted by <a href='${profileURL}'>${senderName}</a> | 
            <time class='date media-meta-date' datetime='${dateTime[0].replace(/\//g, '-')} ${dateTime[1]}'>${messageCreated}</time>
          </div>
          <div class='media-reply'>
            <a href='${postURL}' target='_blank' rel='noopener noreferrer' >View Yammer post</a>
          </div>
        </div>
      </li>`;

      return html;
    }

    const timeDifference = (current: Date, previous: Date): string => {
      let msPerMinute = 60 * 1000;
      let msPerHour = msPerMinute * 60;
      let msPerDay = msPerHour * 24;
      let msPerMonth = msPerDay * 30;
      let msPerYear = msPerDay * 365;
      let elapsed = current.getTime() - previous.getTime();

      if (elapsed < msPerMinute) {
        if (Math.round(elapsed / 1000) > 1) {
          return Math.round(elapsed / msPerMinute) + ' seconds';
        } else {
          return '1 second ago';
        }
      } else if (elapsed < msPerHour) {
        if (Math.round(elapsed / msPerMinute) > 1) {
          return Math.round(elapsed / msPerMinute) + ' minutes ago';
        } else {
          return '1 minute ago';
        }
      } else if (elapsed < msPerDay) {
        if (Math.round(elapsed / msPerHour) > 1) {
          return Math.round(elapsed / msPerHour) + ' hours ago';
        } else {
          return '1 hour ago';
        }
      } else if (elapsed < msPerMonth) {
        if (Math.round(elapsed / msPerDay) > 1) {
          return Math.round(elapsed / msPerDay) + ' days ago';
        } else {
          return '1 day ago';
        }
      } else if (elapsed < msPerYear) {
        if (Math.round(elapsed / msPerMonth) > 1) {
          return Math.round(elapsed / msPerMonth) + ' months ago';
        } else {
          return '1 month ago';
        }
      } else {
        if (Math.round(elapsed / msPerYear) > 1) {
          return Math.round(elapsed / msPerYear) + ' years ago';
        } else {
          return '1 year ago';
        }
      }
    }
  }
  return {
    init,
    launchYammer,
  }
})