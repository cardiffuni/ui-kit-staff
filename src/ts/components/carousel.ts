define(['jquery'], ($: JQueryStatic): UIKit.Module.Carousel => {
  const slideThumbnail = (gallery: HTMLElement): void => {
    if ($(gallery).find('.carousel-indicators.with-thumbnails').length > 0) {
      const margintoSlide = $(gallery).find('.carousel-indicators .active')[0].offsetLeft || 0;
      $(gallery)
        .find('.carousel-indicators')[0].scrollLeft = margintoSlide;
    }
  };
  /**
   * Update the offset for controls and indicators
   * Controls only adjust offset when greater than 768px width.
   *
   * @param {HTMLElement} gallery Gallery item to update offset
   */
  const computeOffset = (gallery: HTMLElement): void => {
    let paginationOffset: number =
      $(gallery)
        .find('.carousel-inner .carousel-item.active img')
        .outerHeight(true) || 0;

    if ($(gallery).find('.carousel-inner .carousel-item.active .carousel-caption').length > 0) {
      $(gallery)
        .find('.carousel-pagination')
        .css({ opacity: '1', top: paginationOffset });
    }

    if (window.matchMedia('(min-width: 768px)').matches) {
      let captionHeight: number =
        $(gallery)
          .find('.carousel-inner .carousel-item.active .carousel-caption')
          .outerHeight(true) || 0;

      const offset =
        captionHeight +
        ($(gallery)
          .find('.carousel-indicators.with-thumbnails')
          .outerHeight(true) || 0);

      $(gallery)
        .find('.carousel-control-prev, .carousel-control-next, .carousel-indicators:not(.with-thumbnails)')
        .each((index: number, item: HTMLElement): void => {
          $(item).css({ opacity: '1', bottom: offset });
        });
    }
  };

  /**
   * Sets the active state for a gallery controls.
   *
   * @param {HTMLElement} gallery Gallery item
   */
  const checkNavigation = (gallery: HTMLElement): void => {
    $(gallery)
      .find('.carousel-item')
      .each((index: number, item: HTMLElement): void => {
        if (index === 0) {
          if ($(item).hasClass('active')) {
            $(gallery)
              .find('.carousel-control-prev')
              .addClass('disabled');
            $(gallery)
              .find('.carousel-control-prev')
              .attr('aria-hidden', 'true');
          } else {
            $(gallery)
              .find('.carousel-control-prev')
              .removeClass('disabled');
            $(gallery)
              .find('.carousel-control-prev')
              .attr('aria-hidden', 'false');
          }
        }

        if (index === $(gallery).find('.carousel-item').length - 1) {
          if ($(item).hasClass('active')) {
            $(gallery)
              .find('.carousel-control-next')
              .addClass('disabled');
            $(gallery)
              .find('.carousel-control-next')
              .attr('aria-hidden', 'true');
            $(gallery)
              .find('.carousel-control-next')
              .attr('tabindex', '-1');
          } else {
            $(gallery)
              .find('.carousel-control-next')
              .removeClass('disabled');
            $(gallery)
              .find('.carousel-control-next')
              .attr('aria-hidden', 'false');
            $(gallery)
              .find('.carousel-control-next')
              .removeAttr('tabindex');
          }
        }
      });
  };

  /**
   * The initialiser for carousel
   */
  const init = (): void => {
    $('.carousel').each((index: number, gallery: HTMLElement): void => {
      const totalItems: number = $(gallery).find('.carousel-item').length;
      $(gallery)
        .find('.carousel-item')
        .each((slideIndex: number, slide: HTMLElement): void => {
          if ($(slide).find('.carousel-caption').length !== 0) {
            $(slide).append(`
              <p class="carousel-pagination"><span class="carousel-pagination-item current">${slideIndex + 1}</span>/${totalItems}</p>`);
          }
        });

      /* Adds indicators when there are none present */
      if ($(gallery).find('.carousel-indicators').length === 0) {
        const generateThumbnails: boolean = $(gallery).data('hasThumbnails') || false;
        let indicatorTemp = `<div class="carousel-indicators-container">
          <ol class="carousel-indicators ${generateThumbnails ? 'with-thumbnails' : ''}">`;

        for (let i = 0; i < totalItems; i++) {
          if (generateThumbnails) {
            const img = $($(gallery).find('.carousel-item img')[i]);
            const src: string | undefined = img.attr('src');

            if (src !== undefined) {
              let url: string = src.split(/[?#]/)[0];

              indicatorTemp += `<li data-target="#${gallery.id}" data-slide-to="${i}" class="${i === 0 ? 'active' : ''}">
                <img
                  srcset="
                    ${url}?w=50&amp;h=28&amp;fit=crop&amp;q=30&amp;auto=format 50w,
                    ${url}?w=88&amp;h=50&amp;fit=crop&amp;q=30&amp;auto=format 88w"
                  src="${url}?w=88&amp;h=50&amp;fit=crop&amp;q=30&amp;auto=format"
                  sizes="(max-width: 350px) 50px, 88px"
                  alt="Thumbnail: ${img.attr('alt')}">
              </li>`;
            } else {
              indicatorTemp += `<li data-target="#${gallery.id}" data-slide-to="${i}" class="${i === 0 ? 'active' : ''}"></li>`;
            }
          } else {
            indicatorTemp += `<li data-target="#${gallery.id}" data-slide-to="${i}" class="${i === 0 ? 'active' : ''}"></li>`;
          }
        }

        indicatorTemp += '</ol></div>';
        $(gallery).append(indicatorTemp);
      }

      checkNavigation(gallery);
      computeOffset(gallery);
    });

    $(document).on('slid.bs.carousel', '.carousel', function(): void {
      computeOffset(this);
      checkNavigation(this);
      slideThumbnail(this);
    });

    $(window).on('resize', (): void => {
      $('.carousel').each((index: number, element: HTMLElement): void => {
        computeOffset(element);
      });
    });
  };

  return {
    init,
    checkNavigation,
    computeOffset
  };
});
