define(['jquery', 'observer'], ($: JQueryStatic, intersect: any): UIKit.Module.LazyLoader => {
  var IntersectionObserver = intersect;

  const init = (): void => {
    let lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));
    let lazyImageObserver = new IntersectionObserver(function(entries: any): void {
      entries.forEach(function(entry: any): void {
        if (entry.intersectionRatio > 0) {
          let lazyImage = $(entry.target) as JQuery<HTMLImageElement>;
          lazyImage.attr('src', lazyImage.attr('data-src') || '');
          lazyImage.attr('srcset', lazyImage.attr('data-srcset') || '');
          lazyImage.removeClass('lazy');
          lazyImageObserver.unobserve(lazyImage[0]);
        }
      });
    });

    lazyImages.forEach(function(lazyImage): void {
      lazyImageObserver.observe(lazyImage);
    });
  };
  
  return {
    init
  };
});
