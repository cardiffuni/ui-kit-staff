define(['jquery'], ($: JQueryStatic): UIKit.Module.SearchForm => {
  /**
   * Initalises all search forms.
   *
   * @requires bootstrap/collapse
   */
  const init = (): void => {
    /**
     * Calcuate the offset of moble nav when the search box is open
     */
    $(document).on('hide.bs.collapse', '.search-form', function(this: HTMLElement): void {
      $('body').removeClass('search-open');
      const offset = ($('#cu-cookie-alert').outerHeight(true) || 0) + ($('.nav-global').outerHeight(true) || 0);
      $('.nav-global-mobile').css('height', `calc(100% - ${offset}px)`);
    });

    /**
     * Calcuate the offset of moble nav when the search box is closed
     */
    $(document).on('show.bs.collapse', '.search-form', function(this: HTMLElement): void {
      $('body').addClass('search-open');
      const offset = ($('#cu-cookie-alert').outerHeight(true) || 0) + 
        ($('.nav-global').outerHeight(true) || 0) + 
        ($('.search-form').outerHeight(true) || 0);
      $('.nav-global-mobile').css('height', `calc(100% - ${offset}px)`);
    });

    /** 
     * Un-focus the input box when the accordion is closed.
     */
    $(document).on('hidden.bs.collapse', '.search-form', function(this: HTMLElement): void {
      $('#query')
        .get(0)
        .blur();
    });

    /** 
     * Focus the input box when the accordion is opened.
     */
    $(document).on('shown.bs.collapse', '.search-form', function(this: HTMLElement): void {
      $('.search-form .search-form-query')[0].focus();
    });

    /** 
     * Adds or removes a class when a search box is not empty
     */
    $(document).on('keyup input', '.search-form-query', function(this: HTMLInputElement): void {
      if (this.value.length > 0) {
        $(this).closest('form').addClass('has-input');
      } else {
        $(this).closest('form').removeClass('has-input');
      }
    });

    /* Add has-input if search is loaded with input */
    $('.search-form-query').each((_, item): void => {
      if ($(item).val() !== '') {
        $(item).closest('form').addClass('has-input')
      }
    })

    /** 
     * Clears the input
     */
    $(document).on('click', '.search-form-clear', function(evt: JQuery.ClickEvent): void {
      evt.preventDefault();
      $(this).closest('form').find('.search-form-query').val('');
      $(this).closest('form').removeClass('has-input');
      $(this).closest('form').find('.search-form-query')[0].focus();
    });
  };

  return {
    init
  };
});
