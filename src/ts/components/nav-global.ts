define(['jquery', './modal-backdrop'], ($: JQueryStatic, backdrop: UIKit.Module.ModalBackdrop): any => {
  class Navigation {
    public pageAssetid: string;
    public storage: UIKit.Navigation.Store.Store;
    public storageName: string;
    public endpoint: string;
    public currentURL: string;
    public status: UIKit.Navigation.Status;
    public transitionDuration: number;
    public transitionScrollUpDruation: number;
    public isLocal: boolean;

    public context: string;
    public constructor() {
      /* Set the page URL and session name */
      this.currentURL = window.location.href;
      this.storageName = 'cusn';
      this.transitionDuration = 350;
      this.transitionScrollUpDruation = 50;
      this.pageAssetid = $('body').data('assetid') || undefined;
      this.context = $('html').attr('lang') || 'en';
      this.isLocal = window.location.href.indexOf('localhost:') !== -1 || window.location.href.indexOf('127.0.0.1') !== -1;

      /* Set the endpoint */
      if (this.isLocal) {
        this.endpoint = 'https://7dd7df87-31b4-4a7d-b264-a42e68d76734.mock.pstmn.io/?id=';
      } else {
        if (this.context === 'cy') {
          this.endpoint = '/cy/feeds/staff/navigation?id=';
        } else {
          this.endpoint = '/feeds/staff/navigation?id=';
        }
      }

      /* Set menu status */
      this.status = {
        session: {
          refresh: this.currentURL.search(/\/_recache/) !== -1,
          disable: this.currentURL.search(/\/_nocache/) !== -1,
        },
        transition: {
          menu: false,
        },
        direction: {
          back: false,
        },
        state: {
          menuOpen: false,
          waitingResponse: false,
          finishedProcessing: false,
        },
      };

      /* Reset the session  if /_recache in url */
      if (this.status.session.refresh) {
        window.sessionStorage.removeItem(this.storageName);
        this.storage = {};
      } else {
        this.storage = JSON.parse(window.sessionStorage.getItem(this.storageName) || '{}');
      }

      /* Ensure there is always an empty context */
      if (this.storage[this.context] === undefined) {
        this.storage[this.context] = {};
      }

      /* Initalise all event handers */
      this.init();

      /* Fetch navigation items for current page */
      this.fetchCurrentPage();
      this.checkDeviceWidth();
    }

    /**
     * Initaliser for the navigation.
     * Add event handler for .nav-back, nav-link-expand, and the menu open button.
     *
     */
    public init = (): void => {
      $(document).on('click', '.nav-global-mobile .nav-link-expand, .nav-global-mobile-header-back', (evt: JQuery.ClickEvent): void => {
        const item: JQuery<HTMLElement> = $(evt.target);
        const assetid: string | undefined = item.attr('data-assetid');

        /* Only move to next screen when a valid assetid is provided */
        if (assetid !== undefined && assetid.length > 0) {
          evt.preventDefault();

          /* Set transition direction */
          this.status.direction.back = item.hasClass('nav-link');

          this.get(assetid);
        }
      });

      /* Sets an event handler for when burger item is clicked */
      $(document).on('click', '#open-navigation', (evt: JQuery.ClickEvent): void => {
        evt.preventDefault();

        this.toggleMenu();
      });

      /* Desktop navigation backdrop */
      $('.nav-global-links').hover(
        (e): void => {
          e.preventDefault();
          backdrop.showBackdrop();
        },
        (e): void => {
          e.preventDefault();
          backdrop.hideBackdrop();
        }
      );
    };

    /**
     * Calls the request when it is not within session.
     * Then calls the render function.
     *
     * @param {string} assetid The assetid of the page to request pages for.
     * @see request
     * @see renderNavigation
     * @returns void
     */
    public get = (assetid: string): void => {
      if (this.storage[this.context][assetid] === undefined || this.status.session.disable) {
        /* Reset processing flag */
        this.status.state.finishedProcessing = false;

        /* Begin transitioning */
        $('.nav-global-mobile').animate({ scrollTop: '0' }, this.transitionScrollUpDruation);
        this.transition('').then((): void => {
          if (!this.status.state.finishedProcessing) {
            this.updateTemplate(
              // eslint-disable-next-line max-len
              `<svg class="icon loading" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><g transform="translate(1 1)" stroke-width="2"><circle class="loading-circle" cx="18" cy="18" r="18"/><path d="M36 18c0-9.94-8.06-18-18-18"><animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s" repeatCount="indefinite"/></path></g></g></svg>`,
              true
            );
          }
        });

        /* Create the API request */
        this.request(assetid).then((json: UIKit.Navigation.Store.Item): void => {
          this.storage[this.context][assetid] = json;

          /* Update the browser session storage*/
          this.updateSession();

          /* Render the new navigation items */
          this.renderNavigation(assetid);
        });
      } else {
        /* When item is in session */

        /* Focus back at the top */

        $('.nav-global-mobile').animate({ scrollTop: '0' }, this.transitionScrollUpDruation);
        this.transition('');

        /* Render the new navigation items */
        this.renderNavigation(assetid);
      }
    };

    /**
     * Updated the browser's session with the new latest storage.
     * Promise is used for async.
     * @see storage
     *
     * @returns Promise<void>
     */
    public updateSession = (): Promise<void> =>
      new Promise((): void => {
        window.sessionStorage.setItem(this.storageName, JSON.stringify(this.storage));
      });

    /**
     * Renders the navigation
     *
     * @param  {string} assetid
     * @returns void
     */
    public renderNavigation = (assetid: string): void => {
      const grandParent: UIKit.Navigation.Store.Page = this.storage[this.context][assetid].lineage[
        this.storage[this.context][assetid].lineage.length - 2
      ];
      let parentLink = '';

      if (this.storage[this.context][assetid].lineage[this.storage[this.context][assetid].lineage.length - 1] !== undefined) {
        /* Add parent item */
        const parent: UIKit.Navigation.Store.Page = this.storage[this.context][assetid].lineage[
          this.storage[this.context][assetid].lineage.length - 1
        ];

        const parentName = parent.showEditingBrackets ? `(( ${parent.name} ))` : parent.name;

        /* Add parent item */
        if (this.storage[this.context][assetid].lineage.length > 2) {
          parentLink = `<h2 class="nav-global-mobile-title"><a href="${parent.url}">${parentName}</a></h2>`;
        } else {
          parentLink = `<h2 class="nav-global-mobile-title">${parentName}</h2>`;
        }
      }
      /* Empty string builder. */
      let template = ``;

      /* Add 'back to' item */
      if (grandParent !== undefined) {
        template += `<div class="nav-global-mobile-header">
          <p><a href="${grandParent.url}" data-assetid="${grandParent.assetid}" class="nav-global-mobile-header-back">
            <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.75 32">
              <title>Chevron left</title>
              <path d="M25.88,28.28L13.59,16,25.88,3.72,22.16,0l-16,16,16,16Z" transform="translate(-6.13 0)"></path>
            </svg>${this.i18n('back')} ${grandParent.name}
          </a></p>
          ${parentLink}
        </div>`;
      }

      /* Add parent and children items */
      if (this.storage[this.context][assetid].lineage[this.storage[this.context][assetid].lineage.length - 1] !== undefined) {
        if (this.storage[this.context][assetid].children.length > 0) {
          template += `<ul class="nav-global-mobile-inner">`;

          /* Loop over every child page */
          this.storage[this.context][assetid].children.forEach((child: UIKit.Navigation.Store.Page): void => {
            template += this.navigationPartial(child);
          });

          template += `</ul>`;
        }
      }

      /* Add sibling sections, only used for the homepage */
      if (this.storage[this.context][assetid].siblings.length !== 0 && this.storage[this.context][assetid].children.length === 0) {
        template += `<ul class="nav-global-mobile-inner">`;
        
        this.storage[this.context][assetid].siblings.forEach((sibling: UIKit.Navigation.Store.Page): void => {
          template += this.navigationPartial(sibling);
        });

        template += `<ul class="nav-global-mobile-inner">`;
      }

      this.status.state.finishedProcessing = true;
      this.updateTemplate(template, false);
    };

    /**
     * Calls the endpoint with the requested assetid
     * @param  {string} assetid Current assetid of the page.
     * @see endpoint
     * @returns Promise<UIKit.Navigation.Store.Item>
     */
    public request = (assetid: string): Promise<UIKit.Navigation.Store.Item> => {
      return new Promise((resolve): void => {
        $.get(`${this.endpoint}${assetid}`).done((data: UIKit.Navigation.Store.Item): void => {
          resolve(data);
        });
      });
    };

    /**
     * Translation function
     *
     * @param attribute Translation key
     * @param contextOverride Context to override with
     */
    public i18n = (attribute: string, contextOverride: string | undefined = undefined): string => {
      const context: string = contextOverride || this.context;

      const translations: UIKit.Translations = {
        en: {
          menu: 'Menu',
          close: 'Close',
          back: 'Back to',
        },
        cy: {
          menu: 'Dewislen',
          close: 'Cau',
          back: 'Nôl i',
        },
      };

      return translations[context][attribute] || '';
    };

    /**
     * Toggles the mobile menu when the menu is not currently transition.
     */
    public toggleMenu = (): void => {
      /* Close search if it's open */
      if ($('body').hasClass('search-open')) {
        $('#search-global').collapse('hide');
      }

      /* Only first event when menu is not still transitioning */
      if (!this.status.transition.menu) {
        if (this.status.state.menuOpen) {
          /* Close mobile menu */
          this.status.transition.menu = true;

          $('#openNavigationText').text(this.i18n('menu'));

          $('#open-navigation').removeClass('menu-open');

          /* Transition menu into view */
          this.transitionTimeout('menu-closing').then((): void => {
            /* Remove transitioning states */
            $('body').removeClass('menu-open');
            $('body').removeClass('menu-closing');
            $('.nav-global-mobile').removeClass('visible');
            $('.nav-global-mobile').removeClass('d-block');
            $('body').css('top', '');

            this.status.transition.menu = false;
            this.status.state.menuOpen = false;

            /* Reset navigation to the page user is currently on */
            if (this.pageAssetid !== undefined) {
              this.renderNavigation(this.pageAssetid);
            }
          });
        } else {
          /* open mobile menu */

          /* Get the offset to the top */
          this.status.transition.menu = true;

          $('.nav-global-mobile').addClass('d-block');
          $('.nav-global-mobile').addClass('visible');
          $('body').css('top', `-${window.scrollY}px`);
          $('#open-navigation').addClass('menu-open');

          /* Allow element to be display block to be before transitioning */
          setTimeout((): void => {
            $('#openNavigationText').text(this.i18n('close'));

            this.transitionTimeout('menu-open').then((): void => {
              this.status.transition.menu = false;
            });

            this.status.state.menuOpen = true;
          }, 10);
        }
      }
    };

    /**
     * Adds a class to the body and sets a delay.
     *
     * @param {string} class name tobe added
     * @returns Promise
     */
    public transitionTimeout = (className: string): Promise<boolean> => {
      return new Promise((resolve): void => {
        $('body').addClass(className);

        setTimeout((): void => {
          resolve(true);
        }, this.transitionDuration);
      });
    };

    /**
     * Adds a class to the body and sets a delay.
     *
     * @param {string} class name tobe added
     * @returns Promise
     */
    public scrollUpTimeout = (): Promise<boolean> => {
      return new Promise((resolve): void => {
        setTimeout((): void => {
          resolve(true);
        }, this.transitionScrollUpDruation);
      });
    };

    public navigationPartial = (item: UIKit.Navigation.Store.Page): string => {
      const name = item.showEditingBrackets ? `(( ${item.name} ))` : item.name;

      if (item.has_children) {
        return `
          <li class="nav-item nav-item-expandable">
            <a href="${item.url}" class="nav-link">${name}</a>
            <a href="#" class="nav-link-expand" data-assetid="${item.assetid}">
              <svg class="icon" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>Expand</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-727.000000, -388.000000)" fill="#383735">
                    <path d="M736.75,388 L736.75,395.25 L744,395.25 L744,397.75 L736.75,397.75 L736.75,405 L734.25,405 L734.25,
                    397.749 L727,397.75 L727,395.25 L734.25,395.249 L734.25,388 L736.75,388 Z"></path>
                  </g>
                </g>
              </svg>
            </a>
          </li>`;
      }
      return `
        <li class="nav-item">
          <a href="${item.url}" class="nav-link ${item.active ? 'active' : ''}">${name}</a>
        </li>`;
    };

    public transition = (template: string = ''): Promise<boolean> => {
      this.status.state.waitingResponse = true;

      return new Promise((resolve): void => {
        $('.nav-global-mobile').prepend(`
          <div class="nav-global-mobile-next ${this.status.direction.back ? 'back' : ''}">
            ${template}
          </div>`);
        $('#mobileNavigationItems').addClass('nav-global-mobile-previous');

        if (this.status.direction.back) {
          $('#mobileNavigationItems').addClass('back');
        }

        /* Allow for display block for transitioning */
        setTimeout((): void => {
          $('.nav-global-mobile-next, .nav-global-mobile-previous').addClass('transitioning');

          setTimeout((): void => {
            $('#mobileNavigationItems').remove();
            $('.nav-global-mobile-next').attr('id', 'mobileNavigationItems');
            $('#mobileNavigationItems').removeClass('nav-global-mobile-next');

            this.status.state.waitingResponse = false;
            $('#mobileNavigationItems').removeClass('transitioning');
            $('#mobileNavigationItems').removeClass('back');
            resolve(true);
          }, this.transitionDuration);
        }, 10);
      });
    };

    /**
     * Updates the mobile menu
     * @param {string} template HTML string of the template to update
     * @param {boolean} withLoading Adds a class when it's a loading template
     */
    public updateTemplate = (template: string, withLoading: boolean): void => {
      if (this.status.state.waitingResponse) {
        $('.nav-global-mobile-next').html(template);
      } else {
        $('#mobileNavigationItems').html(template);
      }

      if (withLoading) {
        $('#mobileNavigationItems').addClass('loading');
      } else {
        $('#mobileNavigationItems').removeClass('loading');
      }
    };

    /**
     * Creates a background request to get the current page navigation and
     * update the session.
     */
    public fetchCurrentPage = (): void => {
      if (this.pageAssetid !== undefined && (this.storage[this.context][this.pageAssetid] === undefined || this.status.session.disable)) {
        this.request(this.pageAssetid).then((json: UIKit.Navigation.Store.Item): void => {
          this.storage[this.context][this.pageAssetid] = json;

          this.updateSession();
        });
      }
    };
    /**
     * Checks whether the menu is open on desktop
     *
     * Fixes issues on table when  changes.
     */
    public checkDeviceWidth = (): void => {
      $(window).on('resize', (): void => {
        if (this.status.state.menuOpen) {
          const width: number | undefined = $(window).width();

          /* Check if it's desktop view */
          if (width !== undefined && width >= 980) {
            this.toggleMenu();
          }
        }
      });
    };
  }

  return {
    Navigation,
  };
});
