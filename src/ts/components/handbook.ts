define(['jquery'], ($: JQueryStatic): UIKit.Module.Handbook => {
  
  const translations: UIKit.Translations = {
    en: {
      all: 'All',
      selectAcSchool: 'Please select an Academic School.',
      selectAcYear: 'Please select an academic year.',
      selectLevel: 'Please select a level.',
      searchError: 'Search error'
    },
    cy: {
      all: 'Pob un',
      selectAcSchool: 'Dewiswch Ysgol Academaidd.',
      selectAcYear: 'Dewiswch flwyddyn academaidd.',
      selectLevel: 'Dewiswch lefel.',
      searchError: 'Gwall chwilio'
    }
  }

  const config = {
    lang: $('html').attr('lang') || 'en',
    apiURL: '//handbooks.data.cardiff.ac.uk/parentschools?callback=?',
    levURL: '//handbooks.data.cardiff.ac.uk/levels?callback=?'
  }

  const svg = {
    // eslint-disable-next-line max-len
    warning: '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.01 32"><title>Warning</title><path d="M19.56,2.22l12,24a3.71,3.71,0,0,1,.44,2,3.89,3.89,0,0,1-.59,1.91A4,4,0,0,1,28,32H4A4,4,0,0,1,.59,30.09,3.89,3.89,0,0,1,0,28.19a3.71,3.71,0,0,1,.44-2l12-24A3.72,3.72,0,0,1,13.91.59a4,4,0,0,1,4.19,0,3.72,3.72,0,0,1,1.47,1.63h0ZM18,19V9a1,1,0,0,0-1-1H15a1,1,0,0,0-1,1V19a1.08,1.08,0,0,0,1,1h2A1,1,0,0,0,18,19Zm0,8V25a1,1,0,0,0-1-1H15a1,1,0,0,0-1,1v2a1.08,1.08,0,0,0,1,1h2A1,1,0,0,0,18,27Z" transform="translate(0 0)" /></svg>'
  }

  const init = (): void => {
    $.ajax({
      url: config.apiURL,
      async: false,
      dataType: 'json',
      success: function(json): void {
        $.each(json.schools, function(index): void {
          $('#school1').append(`<option value="${json.schools[index].parentSchoolId}">${json.schools[index].schoolName}</option>`);
        });
      }
    });

   
    $('#level').append(`<option value="">${translations[config.lang]['all']}</option>`);
    $.ajax({
      url: config.levURL,
      async: false,
      dataType: 'json',
      success: function(json): void {
        $.each(json.levels, function(index): void {
          if(json.levels[index].name !== null) {
            $('#handbook_level').append(`<option value="${json.levels[index].itemcode}">${json.levels[index].name}</option>`);
          }
        });
      }
    });

    $(document).on('click', '.searchHandbook', function(e: JQuery.ClickEvent): void {
      e.preventDefault();
      searchHandbook();
    });
  }

  const searchHandbook = (): void =>{
    const handbookLevel = $('#handbook_level').val();
    const school = $('#school1').val();
    let handYear = $('#handyear').val() || 'Academic year'
    let downloadURL = '';

    $('.alert-error').remove();

    if(school !== 'Academic School' && handbookLevel !== 'Level' && handYear !== 'Academic year') {
      if ((handYear === null) || (handYear === '')) {handYear = 'XX';}
      
      if(handbookLevel !== null && handbookLevel !== undefined) {
        if ((handYear === null) || (handYear === '')) {
          downloadURL = `//handbooks.data.cardiff.ac.uk/schoolmodcatyearlevel/${school}/${handbookLevel}.doc`;
        }
        else {
          downloadURL = `//handbooks.data.cardiff.ac.uk/schoolmodcatyearlevel/${school}/${handYear}/${handbookLevel}.doc`;
        }
      } else if ((handYear === null) || (handYear === '')) {
        downloadURL = `//handbooks.data.cardiff.ac.uk/schoolmodcatyearlevel/${school}.doc`;
      }
      else {
        downloadURL = `//handbooks.data.cardiff.ac.uk/schoolmodcatyearlevel/${school}/${handYear}.doc`;
      }
      window.open(downloadURL, '_self');

    } else {

      let searchErrors = '';

      if (school === 'Academic School') {
        searchErrors += `<li>${translations[config.lang]['selectAcSchool']}</li>`;
      }

      if(handbookLevel == 'Level') {
        searchErrors += `<li>${translations[config.lang]['selectLevel']}</li>`;
      }

      if(handYear === 'Academic year') {
        searchErrors += `<li>${translations[config.lang]['selectAcYear']}</li>`;
      }

      const errorTemplate = `<div class="alert alert-error with-icon">
        <div class="alert-icon">${svg['warning']}</div>
        <div class="alert-body">
          <h2>
            ${translations[config.lang]['searchError']}
          </h2>
          <ul class="list unstyled" style="margin-bottom:0px">${searchErrors}</ul>
        </div>
      </div>`

      $('#handbook-search-legend').after(errorTemplate);
    }
  }

  return {
    init,
    searchHandbook
  }
})