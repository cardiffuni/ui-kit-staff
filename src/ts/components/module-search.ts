define(['jquery'], ($: JQueryStatic): UIKit.Module.ModuleSearch => {
  let config = {
    lang: $('html').prop('lang') || 'en',
    palletUrl: 'https://handbooks.data.cardiff.ac.uk',
  };
  /* eslint-disable max-len */
  const svg = {
    download:
      '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 23.59"><title>Download</title><path d="M30.69,18.88H23.91c0.16-7.28-3-11.37-7.34-12.66s-9.72.22-13.88,4.06C5.53,8.75,9,7.84,11.88,8.84s4.87,3.94,4.78,10h-7l10.53,10.5Z" transform="translate(-2.69 -5.78)" /></svg>',
    'chevron-left':
      '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.75 32"><title>Chevron left</title><path d="M25.88,28.28L13.59,16,25.88,3.72,22.16,0l-16,16,16,16Z" transform="translate(-6.13 0)"></path></svg>',
    warning:
      '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.01 32"><title>Warning</title><path d="M19.56,2.22l12,24a3.71,3.71,0,0,1,.44,2,3.89,3.89,0,0,1-.59,1.91A4,4,0,0,1,28,32H4A4,4,0,0,1,.59,30.09,3.89,3.89,0,0,1,0,28.19a3.71,3.71,0,0,1,.44-2l12-24A3.72,3.72,0,0,1,13.91.59a4,4,0,0,1,4.19,0,3.72,3.72,0,0,1,1.47,1.63h0ZM18,19V9a1,1,0,0,0-1-1H15a1,1,0,0,0-1,1V19a1.08,1.08,0,0,0,1,1h2A1,1,0,0,0,18,19Zm0,8V25a1,1,0,0,0-1-1H15a1,1,0,0,0-1,1v2a1.08,1.08,0,0,0,1,1h2A1,1,0,0,0,18,27Z" transform="translate(0 0)" /></svg>',
  };
  /* eslint-enable max-len */

  const translations: UIKit.Translations = {
    en: {
      school: 'School',
      depCode: 'Department code',
      modCode: 'Module code',
      extSubjCode: 'External subject code',
      numCredits: 'Number of credits',
      level: 'Level',
      langModuleDelivery: 'Language of module delivery',
      modLeader: 'Module leader',
      semester: 'Semester',
      academicYear: 'Academic year',
      assessBreakdown: 'Assessment breakdown',
      type: 'Type',
      qualifyingMark: 'Qualifying mark',
      title: 'Title',
      duration: 'Duration (hrs)',
      download: 'Download',
      backResults: 'Back to results',
      code: 'Code',
      searchError: 'Search error',
      validModCode: 'Please enter a valid module code.',
      selectAcYear: 'Please select an academic year.',
      selectAcSchool: 'Please select an Academic School.',
      selectLevel: 'Please select a level.',
      modName: 'Module name',
      startDate: 'Start date',
      modCodeNotFound:
        'The module code you entered could not be found. If you don\'t know the module code, you can browse modules by Academic School.',
      noModsAvailable: 'There are no modules available using these criteria. Try amending your search.',
    },
    cy: {
      school: 'Ysgol',
      depCode: 'Côd yr adran',
      modCode: 'Côd y modiwl',
      extSubjCode: 'Côd allanol y pwnc',
      numCredits: 'Nifer y credydau',
      level: 'Lefel',
      langModuleDelivery: 'Canran a addysgir drwy <br> gyfrwng y Gymraeg',
      modLeader: 'Arweinydd y modiwl',
      semester: 'Semester',
      academicYear: 'Blwyddyn academaidd',
      assessBreakdown: 'Y potensial ar gyfer ailasesu yn y modiwl hwn',
      type: 'Math',
      qualifyingMark: 'Marc Cymhwyso',
      title: 'Teitl',
      duration: 'Hyd (oriau)',
      download: 'Lawrlwytho',
      backResults: 'Nôl i’r Canlyniadau',
      code: 'Côd',
      searchError: 'Gwall chwilio',
      validModCode: 'Nodwch gôd modiwl dilys.',
      selectAcYear: 'Dewiswch flwyddyn academaidd.',
      selectAcSchool: 'Dewiswch Ysgol Academaidd.',
      selectLevel: 'Dewiswch lefel.',
      modName: 'Enw’r modiwl',
      startDate: 'Dyddiad dechrau',
      // eslint-disable-next-line max-len
      modCodeNotFound: 'Nid oedd modd darganfod modiwl wrth ddefnyddio’r côd a nodwyd gennych. Os nad ydych yn gwybod côd y modiwl, gallwch bori drwy’r modiwlau fesul Ysgol Academaidd.',
      noModsAvailable: 'Nid oes modiwlau ar gael sy’n dilyn y meini prawf hyn. Chwiliwch eto.',
    },
  };

  const init = (): void => {
    const url = `${config.palletUrl}/parentschools?callback=?`;

    $.ajax({
      url: url,
      async: false,
      dataType: 'json',
      success: function (json: any): void {
        $.each(json.schools, function (index: any): void {
          $('#school').append(`<option value="${json.schools[index].parentSchoolId}">${json.schools[index].schoolName}</option>`);
        });
      },
    });

    const levUrl = `${config.palletUrl}/levels?callback=?`;

    $.ajax({
      url: levUrl,
      async: false,
      dataType: 'json',
      success: function (json: any): void {
        $('#level').append('<option value="">All</option>');
        $.each(json.levels, function (index: any): void {
          if (json.levels[index].name !== null) {
            $('#level').append(`<option value="${json.levels[index].itemcode}">${json.levels[index].name}</option>`);
          }
        });
      },
    });

    $(document).on('click', '.searchModule', function (e: any): void {
      e.preventDefault();
      $('#moduleResults').show();
      $('#moduleDetails').hide();
      searchModule($(this).data('search'));
    });

    $(document).on('click', '#school,#modschyear,#level', function (): void {
      $('#module').val('');
      $('#modyear option:eq(0)').prop('selected', true);
    });

    $(document).on('click', '#module,#modyear', function (): void {
      $('#school option:eq(0)').prop('selected', true);
      $('#modschyear option:eq(0)').prop('selected', true);
      $('#level option:eq(0)').prop('selected', true);
    });

    $(document).on('click', '.back-search', function (e: any): void {
      e.preventDefault();
      $('#moduleResults').show();
      $('#moduleDetails').hide();
    });

    $(document).on('click', '.download-info', function (e: any): void {
      e.preventDefault();
      const url = `${config.palletUrl}/module/${$(this).data('module')}/${$(this).data('occurrence')}.doc`;
      window.open(url, 'Download');
    });

    $(document).on('click', '.close', function (): void {
      $('#module').val('');
      $('#modyear option:eq(0)').prop('selected', true);
      $('#school option:eq(0)').prop('selected', true);
      $('#modschyear option:eq(0)').prop('selected', true);
      $('#level option:eq(0)').prop('selected', true);
    });

    $(document).on('click', '.module-item', function (e: any): void {
      e.preventDefault();
      $('#moduleResults').hide();
      $('#moduleDetails').show();

      $('#moduleDetails').empty();
      const moduleCode = $(this).data('module');
      const moduleOccurrence = $(this).data('occurrence');

      const url = `${config.palletUrl}/module/${moduleCode}/${moduleOccurrence}?callback=?`;

      $.ajax({
        url: url,
        async: false,
        dataType: 'json',
        success: function (json: any): void {
          let moduleDetail = `<div class="clearfix">
            <a href="#" class="btn float-right download-info" data-module="${json.module.moduleCode}"
              data-occurrence="${json.occurrences.moduleOccurrence}">
              ${svg['download']}${translations[config.lang]['download']}
            </a> 
            <a href="#" class="btn btn-primary float-right back-search mr-2">
                ${svg['chevron-left']}${translations[config.lang]['backResults']}
            </a>
          </div>
          <h3>${json.module.moduleCode} - ${json.module.moduleName}</h3>
          <div class="table-responsive">
            <table class='table'>
              <tbody>
                <tr><th>${translations[config.lang]['school']}</th><td width='70%'>${json.module.school.schoolName}</td></tr>
                <tr><th>${translations[config.lang]['depCode']}</th><td>${json.module.school.schoolCode}</td></tr>
                <tr><th>${translations[config.lang]['modCode']}</th><td>${json.module.moduleCode}</td></tr>
                <tr><th>${translations[config.lang]['extSubjCode']}</th><td>${json.module.extSubCode}</td></tr>
                <tr><th>${translations[config.lang]['numCredits']}</th><td>${json.module.moduleCredits}</td></tr>
                <tr><th>${translations[config.lang]['level']}</th><td>${json.module.levelCode}</td></tr>`;

          if (json.occurrences !== null && json.occurrences !== '') {
            if (json.occurrences.staff !== null && json.occurrences.staff !== '') {
              moduleDetail += `<tr><th>${translations[config.lang]['langModuleDelivery']}</th><td>`;
              if (config.lang === 'cy') {
                moduleDetail += `${json.occurrences.welshPerc}%`;
              } else {
                moduleDetail += `English`;
              }
              moduleDetail += `</td>
              </tr>
              <tr>
                <th><b>${translations[config.lang]['modLeader']}</b></th>
                <td>${json.occurrences.staff.moduleLeader.title} ${json.occurrences.staff.moduleLeader.firstName} ${
  json.occurrences.staff.moduleLeader.surname
}</td>
              </tr>`;
            }
            moduleDetail += `<tr>
              <th>${translations[config.lang]['semester']}</th><td>${json.occurrences.semester.type}</td>
            </tr>
            <tr>
              <th>${translations[config.lang]['academicYear']}</th><td>${json.occurrences.academicYearCode}</td>
            </tr></tbody></table></div>`;

            const sortdesc = json.descriptions;

            sortdesc.sort(function (a: any, b: any): number {
              return a.rank - b.rank;
            });

            let asses = false;
            $.each(json.descriptions, function (index: number): void {
              if (json.descriptions[index].descriptionDetail !== null && json.descriptions[index].descriptionDetail !== '') {
                if (json.descriptions[index].descriptionName.descriptionCode === 'MAV_ASSM') {
                  if (config.lang === 'cy') {
                    if (json.descriptions[index].descriptionName.welshDescName !== null) {
                      moduleDetail += `<h4>${json.descriptions[index].descriptionName.welshDescName}</h4>`;
                    } else {
                      moduleDetail += `<h4>${json.descriptions[index].descriptionName.descriptionName}</h4>`;
                    }
                  } else {
                    moduleDetail += `<h4>${json.descriptions[index].descriptionName.descriptionName}</h4>`;
                  }
                  moduleDetail += `${json.descriptions[index].descriptionDetail}<h4>${translations[config.lang]['assessBreakdown']}</h4>
                  <div class="table-responsive">
                    <table class='table'>
                      <thead>
                        <th>${translations[config.lang]['type']}</th>
                        <th>%</th>
                        <th>${translations[config.lang]['qualifyingMark']}</th>
                        <th>${translations[config.lang]['title']}</th>
                        <th>${translations[config.lang]['duration']}</th>
                      </thead>
                    <tbody>`;

                  let assessments = json.assessments;
                  $.each(assessments, function (index: number): void {
                    moduleDetail += `<tr>
                      <td>${assessments[index].assessmentType.assessmentType}</td>
                      <td>${assessments[index].percentage}</td>
                      <td>${nullToNA(assessments[index].qualifyingMark)}</td>
                      <td>${assessments[index].name}</td>
                      <td>${nullToNA(assessments[index].examDuration)}</td>
                    </tr>`;
                  });
                  moduleDetail += `</tbody></table></div>`;
                  asses = true;
                } else {
                  if (config.lang === 'cy') {
                    if (json.descriptions[index].descriptionName.welshDescName !== null) {
                      moduleDetail += `<h4>${json.descriptions[index].descriptionName.welshDescName}</h4>`;
                    } else {
                      moduleDetail += `<h4>${json.descriptions[index].descriptionName.descriptionName}</h4>`;
                    }
                  } else {
                    moduleDetail += `<h4>${json.descriptions[index].descriptionName.descriptionName}</h4>`;
                  }
                  moduleDetail += json.descriptions[index].descriptionDetail;
                }
              }
            });

            if (!asses) {
              moduleDetail += `<h4>${translations[config.lang]['assessBreakdown']}</h4>
                <div class="table-responsive">
                  <table class='table'>
                    <thead>
                    <th>${translations[config.lang]['type']}</th>
                    <th>%</th>
                    <th>${translations[config.lang]['qualifyingMark']}</th>
                    <th>${translations[config.lang]['title']}</th>
                    <th>${translations[config.lang]['duration']}</th>
                  </thead>
                  <tbody>`;

              let assessments = json.assessments;

              $.each(assessments, function (index: number): void {
                moduleDetail += `<tr>
                  <td>${assessments[index].assessmentType.assessmentType}</td>
                  <td>${assessments[index].percentage}</td>
                  <td>${nullToNA(assessments[index].qualifyingMark)}</td>
                  <td>${assessments[index].name}</td><td>${nullToNA(assessments[index].examDuration)}</td>
                </tr>`;
              });
              moduleDetail += `</tbody></table></div>`;
            }
          }
          $('#moduleDetails').append(moduleDetail);
        },
      });
    });
  };

  const searchModule = (type: string): void => {
    let moduleid = $('#module').val() || '';

    if (moduleid !== '') {
      moduleid = moduleid.toString().toUpperCase();
    }

    $('#moduleResults').empty();
    $('.alert-error').remove();

    let errorTemplate = `<div class="alert alert-error with-icon">
      <div class="alert-icon">
      ${svg['warning']}
      </div>
      <div class="alert-body">  
        <h3>${translations[config.lang]['searchError']}</h3>
        <ul class="list" >`;

    switch (type) {
      case 'module':
        const moduleVal = $('#module').val();
        const moduleYear = $('#modyear').val();

        if (moduleVal !== '' && moduleYear !== 'Academic year') {
          findModuleIndiv(moduleid);
        } else {
          if (moduleVal === '') {
            errorTemplate += `<li>${translations[config.lang]['validModCode']}</li>`;
          }
          if (moduleYear === 'Academic year') {
            errorTemplate += `<li>${translations[config.lang]['selectAcYear']}</li>`;
          }
          $('#module-search-legend').after(`${errorTemplate}</ul></div>`);
        }
        break;

      case 'school':
        const school = $('#school').val();
        const level = $('#level').val();
        const academicYear = $('#modschyear').val();

        if (school !== 'Academic School' && level !== 'Level' && academicYear !== 'Academic year') {
          findModuleSch();
        } else {
          if (school === 'Academic School') {
            errorTemplate += `<li>${translations[config.lang]['selectAcSchool']}</li>`;
          }
          if (level === 'Level') {
            errorTemplate += `<li>${translations[config.lang]['selectLevel']}</li>`;
          }
          if (academicYear === 'Academic year') {
            errorTemplate += `<li>${translations[config.lang]['selectAcYear']}</li>`;
          }
          $('#school-search-legend').after(`${errorTemplate}</ul></div></div>`);
        }
        break;
    }
  };

  const findModuleIndiv = (moduleid: any): void => {
    $('#moduleResults').empty();
    const modyear = $('#modyear').val();
    let moduleItem = '';

    if (moduleid !== null && moduleid !== '') {
      const url = `${config.palletUrl}/moduleoccs/${moduleid}/${modyear}?callback=?`;

      $.getJSON(url, function (json: any): void {
        let results = json.modOccurrences;

        if (results.length > 0) {
          moduleItem += `<div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th>${translations[config.lang]['code']}</th>
                  <th>${translations[config.lang]['modName']}</th>
                  <th>${translations[config.lang]['startDate']}</th>
                </tr>
              </thead>
              <tbody>`;

          $.each(results, function (index: number): void {
            moduleItem += `<tr>
              <td><a href='' class='module-item' data-module='${moduleid}' data-occurrence='${results[index].moduleOccurrence}''>${moduleid}</a></td>
              <td><a href='' class='module-item' data-module='${moduleid}' data-occurrence='${results[index].moduleOccurrence}''>
                ${results[index].modOccName}
              </a></td>
              <td>${results[index].dateOfFirstClass}</td>
            </tr>`;
          });
          moduleItem += `</tbody></table></div>`;
        } else {
          moduleItem += `<div class="alert alert-error with-icon">
            <div class="alert-icon">
              ${svg['warning']}
            </div>
            <div class="alert-body">
              <h3>${translations[config.lang]['searchError']}</h3>
              <p>${translations[config.lang]['modCodeNotFound']}</p>
            </div>
          </div>`;
        }

        $('#moduleResults').append(moduleItem);
      });
    }
  };

  const findModuleSch = (): void => {
    $('#moduleResults').empty();
    let school = $('#school').val();
    let level = $('#level').val();
    let modschyear = $('#modschyear').val();

    if (modschyear === null || modschyear === '') {
      modschyear = '!';
    }

    if (school !== null && school !== '') {
      const url = `${config.palletUrl}/modulesrunning/${school}/${modschyear}/${level}?callback=?`;

      $.getJSON(url, function (json: any): void {
        let moduleListOutput = '';

        if (json.modRunning.length > 0) {
          moduleListOutput += `<div class="table-responsive">
            <table class="table">
            <thead>
              <tr><th>${translations[config.lang]['code']}</th>
                <th>${translations[config.lang]['modName']}</th>
                <th>${translations[config.lang]['startDate']}</th>
              </tr>
            </thead>
            <tbody>`;

          $.each(json.modRunning, function (index: number): void {
            const modid = json.modRunning[index].moduleCode;

            moduleListOutput += `<tr>
              <td><a href='' class='module-item' data-module='${modid}' data-occurrence='${json.modRunning[index].moduleOccurrence}'>${modid}</a></td>
              <td><a href='' class='module-item' data-module='${modid}' data-occurrence='${json.modRunning[index].moduleOccurrence}'>
                ${json.modRunning[index].modOccName}
              </a></td>
              <td>${json.modRunning[index].dateOfFirstClass}</td>
            </tr>`;
          });
          moduleListOutput += '</tbody></table></div>';
        } else {
          moduleListOutput += `<div class="alert alert-error with-icon">
            <div class="alert-icon">
              ${svg['warning']}
            </div>
            <div class="alert-body">
              <h3>${translations[config.lang]['searchError']}</h3>
              <p>${translations[config.lang]['noModsAvailable']}</p>
            </div>
          </div>`;
        }
        $('#moduleResults').append(moduleListOutput);
      });
    }
  };

  const nullToNA = (val: number): string => {
    if (val === undefined || val === null) {
      return 'N/A';
    } else {
      return val.toString();
    }
  };

  return {
    init,
    searchModule,
    findModuleSch,
    findModuleIndiv,
    nullToNA,
  };
});
