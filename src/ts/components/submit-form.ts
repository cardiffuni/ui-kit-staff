define(['jquery'], ($: JQueryStatic): UIKit.Module.SubmitForm => {
  const init = (): void => {
    let lang = $('html').attr('lang') || 'en';

    $('.form').submit(function(): void {
      const submit = $(this).find('input[type="submit"]')

      if (!($(submit).data('submitted') === '1')) {
        $(submit).attr('data-submitted', '1');
        $(submit).css('pointer-events', 'none');
        $(submit).css('background', '#d3d3d2');
        $(submit).blur();

        if (lang === 'cy') {
          $(submit).val('Cyflwyno');
        } else {
          $(submit).val('Submitting');
        }
      }
    });
  };
  return {
    init
  };
});