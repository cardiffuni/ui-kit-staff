define(['jquery'], ($: JQueryStatic): UIKit.Module.Tab => {
  const init = (): void => {
    //if there are any tab panes in page
    if ($('.tab-pane.squiz-bodycopy').length) {
      //group tab panes that are next to each other § wrap
      $('.tab-pane.squiz-bodycopy')
        .filter(function(): boolean {
          return !$(this)
            .prev()
            .is('.tab-pane.squiz-bodycopy');
        })
        .map(function(this): JQuery<HTMLElement> {
          return $(this)
            .nextUntil(':not(.tab-pane.squiz-bodycopy)')
            .addBack();
        })
        .wrap('<div class="tab-content squiz-tabs" />');
      

      //loop through grouped tab panes
      $('.tab-content.squiz-tabs').each(function(tabIndex): void {
        //set up letiable to store tabs html
        let tabsHtml = '';

        
        //loop through tab panes in this group
        $(this)
          .find('.tab-pane.squiz-bodycopy')
          .each(function(index): void {
            $(this)
              .attr('role', 'tabpanel')
              .attr('aria-labelledby', `tab-${tabIndex}` )
              .attr('id', `tab-${$(this).attr('id')}`)
            
            if(index === 0) {
              $(this).addClass('active');
            }

            let descString = $(this).attr('desc') || '';
            tabsHtml += `<li class="nav-item">
              <a href="#${this.id}" class="nav-link${index === 0 ? ' active':''}"
                role="tab" data-toggle="tab" id="tab-${index}" aria-controls="${this.id}" aria-selected="${index === 0 ? true:false}">
                ${extractLanguageSubstring(descString)}
              </a>
            </li>`;           
          });
        //build tabs navigation
        tabsHtml = `<ul class="nav nav-tabs">${tabsHtml}</ul>`;
        //put tabs before this group
        $(this).before(tabsHtml);
      });
    } // end check for .tab-pane
  };

  /**
   * Extracts a value from a special string based on the current language. 
   * Copes if a semi-colon is at the end. 
   * Gets current language from the lang-en attribute on html tag
   * @param inputString Expected string format is 'en=yyy;cy=xxx;cn=zzz'
   */
  const extractLanguageSubstring = (inputString: string): void => {
    // prepare a string to return
    let outputString;

    // set default language to English
    let languageCode = $('html').attr('lang') || '';

    //check whether this is a normal string or a semi-colon separated multilingual string
    let isMultilingual = false;
    if (inputString.indexOf('|') > -1) {
      isMultilingual = true;
    }

    // if special multilingual string, process that bad boy
    // otherwise return the boring normal one
    if (isMultilingual === true) {
      // remove trailing pipe characters
      let trailingSemicolon = /\|$/;
      inputString = inputString.replace(trailingSemicolon, '');

      // split languages
      let arrLanguages = inputString.split('|');

      // put the languages into an object
      let languageStrings: any = {};
      arrLanguages.forEach(function(languageItem): void {
        let arrLanguageItem = languageItem.split('=');
        languageStrings[arrLanguageItem[0]] = arrLanguageItem[1];
      });

      // return the appropriate string for the current language
      outputString = languageStrings[languageCode];
    } else {
      // return it, just normal
      outputString = inputString;
    }

    return outputString;
  };

  return {
    init,
    extractLanguageSubstring,
  };
});
