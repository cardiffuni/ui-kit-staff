define(['jquery'], ($: JQueryStatic): UIKit.Module.WPFeed => {
  const translations: UIKit.Translations = {
    en: {
      loading: 'Loading',
    },
    cy: {
      loading: 'Yn llwytho',
    },
  };

  const init = (): void => {
    const context: string = $('html').prop('lang') || 'en';

    $('.wpfeed-link').each(function (): void {
      let wpfeedLink = $(this);
      let dataLoadingHeight: string | undefined = '100';

      const loadingHTML = `<div class="well loading">
        <p class="loading-text" style="text-align:center; margin:0; line-height: ${dataLoadingHeight}px;">${translations[context].loading}...</p>
      </div>`;

      if (wpfeedLink.is('[data-loadingheight]')) {
        dataLoadingHeight = $(this).attr('data-loadingheight');
      }
      wpfeedLink.css('display', 'none');
      wpfeedLink.wrap('<div class="wpfeed">');
      wpfeedLink.before(loadingHTML);
    });

    $('.wpfeed-link').each(function (): void {
      let wpfeedLink: JQuery<HTMLElement> = $(this);

      const numberofresults = wpfeedLink.attr('data-numberofresults') || 3;
      const category = wpfeedLink.attr('data-category') || '';
      const template = wpfeedLink.attr('data-template') || 'teaser-small';
      const title = wpfeedLink.attr('data-title') || '';
      const titleIcon = wpfeedLink.attr('data-titleicon') || '';
      const titleURL = wpfeedLink.attr('data-titleurl') || '';

      if (wpfeedLink.is('[data-url]') && wpfeedLink.attr('data-url') !== '') {
        const URL = $(this).attr('data-url') || '';

        const JSONurl = `https://public-api.wordpress.com/rest/v1/sites/${URL}/posts/?number=${numberofresults}&category=${category}`;
        JSONloader(JSONurl, wpfeedLink, template, title, titleIcon, titleURL);
      }
    });
  };

  const buildTeasersmall = (wpfeedLink: JQuery<HTMLElement>, JSON: UIKit.WpFeedItem): void => {
    let stringBuilder = '';
    for (let i = 0, length = JSON.posts.length; i < length; i++) {
      stringBuilder += `<section class="teaser teaser-small">
        <div class='teaser-body'>
          <h1 class='teaser-title'><a href="${JSON.posts[i].URL}">${JSON.posts[i].title}</a></h1>
          <p class='teaser-date'>${formatDate(JSON.posts[i].date)}</p>
        </div>
      </section>`;
    }
    wpfeedLink.before(stringBuilder);
    wpfeedLink.siblings('.loading').remove();
    wpfeedLink.remove();
  };

  const buildTeaser = (wpfeedLink: JQuery<HTMLElement>, JSON: UIKit.WpFeedItem): void => {
    let stringBuilder = '';
    for (let i = 0, length = JSON.posts.length; i < length; i++) {
      stringBuilder += `<section class="teaser">
        <div class='teaser-body'>
          <h1 class='teaser-title'><a href='${JSON.posts[i].URL}'>${JSON.posts[i].title}"</a></h1>
          <p class='teaser-date'>${formatDate(JSON.posts[i].date)}</p>
        </div>
      </section`;
    }
    wpfeedLink.before(stringBuilder);
    wpfeedLink.siblings('.loading').remove();
    wpfeedLink.remove();
  };

  const buildTeaservertical = (
    wpfeedLink: JQuery<HTMLElement>,
    JSON: UIKit.WpFeedItem,
    dataTitle: string,
    dataTitleIcon: string,
    dataTitleURL: string
  ): void => {
    let stringBuilder = '';
    for (let i = 0, length = JSON.posts.length; i < length; i++) {
      stringBuilder += `<section class="teaser teaser-vertical">
        <div class="teaser-body">`;

      if (dataTitle !== '' && dataTitleIcon !== '') {
        stringBuilder += `<p class="teaser-category">
          <span class="${dataTitleIcon}"></span>
          <a href="${dataTitleURL}">${dataTitle}</a>
        </p>`;
      } else if (dataTitle !== '') {
        stringBuilder += `<p class="teaser-category"><a href="${dataTitleURL}">${dataTitle}</a></p>`;
      }

      stringBuilder += `<h1 class="teaser-title"><a href="${JSON.posts[i].URL}">${JSON.posts[i].title}'</a></h1>
          <p class="teaser-date">${formatDate(JSON.posts[i].date)}</p>
        </div>
      </section>`;
    }
    wpfeedLink.before(stringBuilder);
    wpfeedLink.siblings('.loading').remove();
    wpfeedLink.remove();
  };

  const formatDate = (date: string): string => {
    let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let formattedDate = new Date(date);
    let d = formattedDate.getDate();
    let m = formattedDate.getMonth();
    let y = formattedDate.getFullYear();
    let newMonth: string = months[m];

    return d + ' ' + newMonth + ' ' + y;
  };

  const JSONloader = (
    url: string,
    wpfeedLink: JQuery<HTMLElement>,
    template: string,
    dataTitle: string,
    dataTitleIcon: string,
    dataTitleURL: string
  ): void => {
    $.ajax({
      async: true,
      global: false,
      url: url,
      dataType: 'json',
      success: function (data): void {
        const JSON = data;

        switch (template) {
          case 'teaser-small':
            buildTeasersmall(wpfeedLink, JSON);
            break;
          case 'teaser':
            buildTeaser(wpfeedLink, JSON);
            break;
          case 'teaser-vertical':
            buildTeaservertical(wpfeedLink, JSON, dataTitle, dataTitleIcon, dataTitleURL);
            break;
        }
      },
    });
  };

  return {
    init,
    buildTeasersmall,
    buildTeaser,
    buildTeaservertical,
    formatDate,
    JSONloader,
  };
});
