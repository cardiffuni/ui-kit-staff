define(['jquery', 'embedly'], ($: JQueryStatic): UIKit.Module.Embed => {
  const wind: UIKit.EmbedWindow = window;
  const embedlykey = 'a785af81d09d43b68a2b466a9fd6c6db';

  const init = (): void => {
    $('a.embed').each(function(): void {
      // only links with class of wind.embedly should be used
      let embedURL = $(this).prop('href');
      switch (true) {
        case embedURL.indexOf('hml.cardiff.ac.uk') != -1: // CardiffPlayer
          cardiffPlayer($(this));
          break;
        case embedURL.indexOf('twitter.com') != -1: // Twitter
          // load the twitter widget file
          wind.twttr = (function(d, s, id): void {
            let js: HTMLScriptElement;
            let fjs = d.getElementsByTagName(s)[0];
            let t = wind.twttr || {};

            if (d.getElementById(id)) {return;}
            js = d.createElement(s) as HTMLScriptElement;
            js.id = id;
            js.src = 'https://platform.twitter.com/widgets.js';

            if (fjs.parentNode !== null) {
              fjs.parentNode.insertBefore(js, fjs);
            }

            t._e = [];
            t.ready = function(f: string): void {
              t._e.push(f);
            };
            return t;
          })(document, 'script', 'twitter-wjs');

          // call it
          twitter($(this));

          break;
        case embedURL.indexOf('xerte.cardiff.ac.uk') != -1: // xerte
          xerte($(this));
          break;
      }
    });

    wind.embedly('defaults', {
      cards: {
        key: embedlykey
      }
    });

    //append styles to remove header and fix minimum width of a wind.embedly card
    // eslint-disable-next-line max-len
    $('head').append('<style class="embedly-css">#cards, .embedly-card, .embedly-card-hug{min-width: 100% !important;}.card .hdr {display: none;visibility: hidden;}.card .action{background: #d4374a;color: #fff;border: none;-webkit-border-radius: 2px; -moz-border-radius: 2px; border-radius: 2px;text-shadow: none; font-family: "franklin_gothic_fs_medregular",Arial,"Helvetica Neue",sans-serif; line-height: 19px; padding: 12px 12px 10px 12px; width: auto; display: inline-block;}.card .action:hover, .card .action:focus, .card .action:active{color: #fff; background-color: #e27683;}</style>' );

    //prevent share icons
    $('.embed').attr('data-card-controls', '0');

    // Call and render card
    wind.embedly('card', '.embed');

    wind.embedly('on', 'card.rendered', function(card: HTMLElement): void {
      let c = $(card)
        .contents()
        .find('body .card .bd .txt-bd');
      //check for panopto
      let url = c.find('.action').prop('href');

      if (url.indexOf('panopto.eu') != -1) {
        let d = $(card).closest('.embedly-card-hug');
        let e = $(card).closest('.embedly-card');
        e.remove();
        d.append(`
          <div class="well panopto-no-video text-center">
            <div class="row">
              <p>This video is not available because the author has restricted access to it.</p>
              <a href="${url}" target="_blank" class="btn btn-primary">View on Panopto</a>
            </div>
          </div>`);
      } else {
        c.find('.action').text(`Continue reading on ${c.find('.title').text()}`);
      }
    });
  };

  const cardiffPlayer = ($this: JQuery<HTMLElement>): void => {
    let embedURL = $this.prop('href');
    let embedURLSplit = embedURL.split('/');
    let embedVideoID = embedURLSplit[4];

    if (embedVideoID == undefined) {
      $this.wrap('<div class="alert"></div>');
      $this.prepend(`Error: Missing video ID. (URL: ${embedURL}, ID: ${embedVideoID})`);
    } else {
      $this.wrap(
        `<figure class="video video ${embedVideoID}>
          <div class="video-body"></figcaption>
        </figure>`
      );

      let videoHeight = $('.video' + embedVideoID).height();
      let videoWidth = $('.video' + embedVideoID).width();

      $this.wrap(
        // eslint-disable-next-line max-len
        `<iframe src="https://hml.cardiff.ac.uk/player?autostart=n&fullscreen=y&width=${videoWidth}&height=${videoHeight}&videoId=${embedVideoID}&quality=hi&captions=n&chapterId=0"></iframe>`
      );
    }
  };

  const twitter = ($this: JQuery<HTMLElement>): void => {
    const embedURL = $this.prop('href');
    const tweetID = embedURL.match(/[\d]+$/);

    $this.wrap(`<div class="embedded embedded-twitter" data-tweetid="'${tweetID}"></div>`);
    $(`.embedded.embedded-twitter[data-tweetid="${tweetID}"]`)
      .addClass($this.attr('class') || '')
      .removeClass('embed');

    $this.remove();

    wind.twttr.ready(function(twttr: any): void {
      twttr.widgets.createTweet(tweetID[0], $('.embedded.embedded-twitter[data-tweetid="' + tweetID + '"]').get(0));
    });
  };

  const xerte = ($this: JQuery<HTMLElement>): void => {
    let embedURL = $this.prop('href');
    let embedURLSplit = embedURL.split('/');
    let embedXerteID = embedURLSplit[3];

    $this.wrap(`<div class="embedded embedded-xerte" id="xerte-${embedXerteID}">`);

    let xerteWidth: number = $('#xerte-' + embedXerteID).width() || 0;
    let xerteHeight = (xerteWidth / 4) * 3;

    $this.wrap(`<iframe src="${embedURL}" width="${xerteWidth}" height="${xerteHeight}"></iframe>'`);
  };
  return {
    init,
    cardiffPlayer,
    twitter,
    xerte
  };
});
