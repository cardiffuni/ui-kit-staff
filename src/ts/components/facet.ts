
define(['jquery'], ($: JQueryStatic): UIKit.Module.Facet => {
  /* eslint-disable max-len */
  const svg = {
    chevronUp: '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 19.75"><title>Chevron up</title><path d="M28.28,25.88L16,13.59,3.72,25.88,0,22.16l16-16,16,16Z" transform="translate(0 -6.13)" /></svg>',
    chevronDown: '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 19.75"><title>Chevron down</title><path d="M28.28,6.13L16,18.41,3.72,6.13,0,9.84l16,16,16-16Z" transform="translate(0 -6.13)"></path></svg>'
  }
  /* eslint-enable max-len */

  const init = (): void => {
    $('.facet-popup').appendTo('body'); 

    $('.facet-show-more').on('click', function(e): void {
      showMoreFacetItems(e, $(this));
    });

    $(document).on('click', '.facet-popup .facet-title .icon', function(e): void {
      closeFacetPopup(e);
    });

    $(document).on('click', '.facet-action-cancel', function(e): void {
      facetCancelAction(e);
    });

    $(document).on('click', '.facet-group-title', function(e): void {
      facetGroupTitleClick(e, $(this));
    });

    $(document).on('click', '.facet-title[data-toggle="collapse"]', function(): void {
      swapChevron($(this));
    });

    $(document).on('click', '#launch-facet-menu-popup', function(e): void {
      launchFilterPopupMenu(e);
    });

    $(document).on('keydown', '.facet .daterange input', function(e): void {
      dateRangeInputFieldFilter(e);
    });

    $(document).on('keyup', '.facet .daterange input', function(e): void {
      moveToNextInput(e, $(this));
    });

    $(document).on('collapse','.facet-body', function(): void {
      hiddenItems($(this));
    });
    
    $(document).on('show','.facet-body', function(): void {
      shownItems($(this));
    });
  }

  /**
   * Check if the input box is empty and if the keypress was: delete or backspace.
   * @param e KeyUpEvent
   * @param $this Jquery input element
   */
  const moveToNextInput = (e: JQuery.KeyUpEvent, $this: JQuery<HTMLInputElement>): void => {
    const value = $this.val() as string;
    if (value !== undefined) {
      if (value.length === 0 && (e.keyCode == 46 || e.keyCode == 8)) {
        //Jump to the previous input box
        $($this).prevAll('.date').eq(0).focus();
      } else if (
        //Check if the input box is full
        value.length === ($($this).attr('maxlength') || 150) &&
        //Ensure the input is a number
        ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105))
      ) {
        //Do not use JS to swap input boxes if the keypress is a tab. Default behaviour will do this.
        if (e.keyCode != 9) {
          //Jump to the next input
          $($this).nextAll('.date').eq(0).focus();
        }
      }
    }
  };

  /**
   * Function modified from: https://stackoverflow.com/questions/469357/html-text-input-allows-only-numeric-input
   * Allow: delete, backspace, escape, enter, and tab.
   * @param e KeyDownEvent
   */
  const dateRangeInputFieldFilter = (e: JQuery.KeyDownEvent): void => {
    if (
      $.inArray(e.keyCode, [46, 8, 27, 13, 9]) !== -1 ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)
    ) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  };

  const showMoreFacetItems = (e: JQuery.ClickEvent, element: JQuery<HTMLElement>): void => {
    e.preventDefault();
    var facetID = $(element).data('facet');
    $(`#${facetID} .facet-items .facet-item.d-none`).removeClass('d-none');
    $(element).remove();
  }

  const closeFacetPopup = (e: JQuery.ClickEvent): void => {
    e.preventDefault();
    $('#facet-menu-popup').addClass('d-none');
    $('body')
      .children()
      .not('script, noscript, .facet-popup, #facet-menu-popup, .nav-global')
      .removeClass('d-none');
  }

  const facetCancelAction = (e: JQuery.ClickEvent): void => {
    e.preventDefault();
    $('.facet-popup').addClass('d-none');
    $('#facet-menu-popup').removeClass('d-none');
  }

  const facetGroupTitleClick = (e: JQuery.ClickEvent, element: JQuery<HTMLElement>): void => {
    e.preventDefault();
    $('#facet-menu-popup').addClass('d-none');
    $('#' + $(element).data('facet')).removeClass('d-none');
  }

  const swapChevron = (element: JQuery<HTMLElement>): void => {
    const body = element.parents('.facet').find('.facet-body')
    if (!body.hasClass('show')) {
      element.find('.icon').replaceWith(svg.chevronDown)
    } else {
      $(element).find('.icon').replaceWith(svg.chevronUp);
    } 
  }

  const launchFilterPopupMenu = (e: JQuery.ClickEvent): void => {
    e.preventDefault();
    $('#facet-menu-popup').removeClass('d-none');
    $('body')
      .children()
      .not('script, noscript, .facet-popup, #facet-menu-popup, .nav-global')
      .addClass('d-none');
  }

  const hiddenItems = (element: JQuery<HTMLElement>): void => {
    var $link = element.find('a,input,button');
    ($link).attr('tabindex','-1');
  }

  const shownItems = (element: JQuery<HTMLElement>): void => {
    var link = element.find('a,input,button');
    link.attr('tabindex','0');
  }

  return {
    init,
    moveToNextInput,
    dateRangeInputFieldFilter,
    showMoreFacetItems,
    closeFacetPopup,
    facetCancelAction,
    facetGroupTitleClick,
    swapChevron,
    launchFilterPopupMenu,
    hiddenItems,
    shownItems
  }

});