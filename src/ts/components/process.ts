define(['jquery'], ($: JQueryStatic): UIKit.Module.Process => {

  const init = (): void => {
    const processList = $('.process');

    processList.each(function(): void {
      const process = $(this);
      const bp = process.data('desktop-bp') ? process.data('desktop-bp') : '(min-width: 768px)';

      setHeight(process, bp);

      var to: any = false
      window.addEventListener('resize', (): void => {
        if (to !== false) {
          clearTimeout(to);
        }

        to = setTimeout((): void => {
          setHeight(process, bp)
        }, 50);
      });
    });
  }

  const setHeight = (process: JQuery<HTMLElement>, bp: string): void => {
    var titles = process.find('.process-step-title');
    let height: any = 'auto';
    let heightNumber = 0

    titles.height('auto');

    if (window.matchMedia(bp).matches) {
      titles.each(function(): void {
        const elHeight = $(this).height();

        if (elHeight !== undefined) {
          if (elHeight > heightNumber) {
            heightNumber = elHeight;
            height = elHeight.toString();
          }
        }
      })
    } 
    titles.height(height);
  }

  return {
    init,
    setHeight
  }
})