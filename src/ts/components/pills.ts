define(['jquery'], ($: JQueryStatic): UIKit.Module.Pills => {
  /**
   * Opens all pills
   */
  const openAll = (): void => {
    $('.filter-pane')
      .addClass('d-block')
      .removeClass('d-none');

    $('.hidden-filtered')
      .addClass('d-block')
      .removeClass('d-none');

    $('.filter-content-title')
      .addClass('d-block')
      .removeClass('d-none');
  };

  /**
   * Open a specific pill
   * @param {string} element Target element
   */
  const open = (element: string): void => {
    $('.filter-pane')
      .addClass('d-none')
      .removeClass('d-block');

    $('.hidden-filtered')
      .addClass('d-none')
      .removeClass('d-block');

    $(element)
      .addClass('d-block')
      .removeClass('d-none');

    $('.filter-content').each(function(): void {
      if ($(this).find('.filter-pane.d-block').length == 0) {
        $(this)
          .find('.filter-content-title')
          .addClass('d-none')
          .removeClass('d-block');
      } else {
        $(this)
          .find('.filter-content-title')
          .addClass('d-block')
          .removeClass('d-none');
      }
    });
  };

  /**
   * The initialiser for pills
   */
  const init = (): void => {
    $('.nav.nav-pills[data-action="filter"] li').on('click', function(evt: JQuery.ClickEvent): void {
      evt.preventDefault();
      const target: string | undefined = $(this).find('a').data('target');

      if (target == 'all') {
        openAll();
      } else if (target !== undefined) {
        open(target);
      }

      const nav = $(this).parent()
      const elm = $(this).find('.nav-link');

      nav.find('.nav-link.active').removeClass('active')
      elm.addClass('active');

      nav.find('.nav-link:not(active)').attr('aria-selected', 'false');
      elm.attr('aria-selected', 'true');
    });
  };

  return {
    init,
    openAll,
    open
  };
});