define(['jquery'], ($: JQueryStatic): UIKit.Module.ModalBackdrop => {
  /**
   * Create overlay markup at the bottom of the page.
   */
  const createBackdrop = (): void => {
    const body = $('body');
    if (body.length) {
      body.append(`<div id="modal-backdrop" class="fade"></div>`);
    }
  };

  /**
   * Show the overlay (fade in with css)
   */
  const showBackdrop = (): void => {
    $('#modal-backdrop').addClass('modal-backdrop show');
  };

  /**
   * Hide the current overlay
   */
  const hideBackdrop = (): void => {
    $('#modal-backdrop').removeClass('show');

    setTimeout((): void => {
      $('#modal-backdrop').removeClass('modal-backdrop');
    }, 150);
  };

  return {
    hideBackdrop,
    showBackdrop,
    createBackdrop,
  };
});
