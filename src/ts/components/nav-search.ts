define(['jquery', './modal-backdrop'], ($: JQueryStatic, backdrop: any): UIKit.Module.NavGlobal => {
  const init = (): void => {
    backdrop.createBackdrop();

    $('#search-global').on('show.bs.collapse', function (): void {
      toggleClasses();

      /* Close menu */
      if ($('body').hasClass('menu-open')) {
        $('#open-navigation').trigger('click');
      }
      backdrop.showBackdrop();
    });

    $('#search-global').on('shown.bs.collapse', function (): void {
      $('.header-global-mobile-search .search-form-query').focus();
    });

    $('#search-global').on('hide.bs.collapse', function (): void {
      toggleClasses();
      backdrop.hideBackdrop();
    });

    checkDeviceWidth();
  };

  /**
   * Toggles body and search toggle elements.
   */
  const toggleClasses = (): void => {
    $('body').toggleClass('search-open');
    $('#search-global-toggle').toggleClass('search-open');
  };

  /**
   * Checks whether the menu is open on desktop
   *
   * Fixes issues on table when  changes.
   */
  const checkDeviceWidth = (): void => {
    $(window).on('resize', (): void => {
      if ($('body').hasClass('search-open')) {
        const width: number | undefined = $(window).width();

        /* Check if it's desktop view */
        if (width !== undefined && width >= 980) {
          $('#search-global').collapse('hide');
        }
      }
    });
  };

  return {
    init,
  };
});
