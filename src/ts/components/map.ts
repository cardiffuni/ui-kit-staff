define(['jquery', 'leaflet'], ($: JQueryStatic, L: any): UIKit.Module.Map => {
  const markerIcon = L.icon({
    iconUrl: 'https://cardiff.imgix.net/__data/assets/image/0005/1746248/leaflet-marker-icon.png?w=24',
    iconSize: [24, 35],
    iconAnchor: [12, 35]
  });

  const init = (): void => {
    $('.map-link').each(function(index): void {
      const classes = $(this).prop('class');
      const query = $(this).prop('href');
      let caption = '';

      if (typeof $(this).data('caption') != 'undefined') {
        caption = $(this).data('caption');
      }

      const coords = getParameterByName('q', query).split(',');
      const lat = parseFloat(coords[0]);
      const lng = parseFloat(coords[1]);

      const zoom = parseInt(getParameterByName('z', query));

      let html = `
        <figure class="map ${classes}">
          <div class="map-body">
            <div id="map${index}" style="height:100%;"></div>
          </div>
      `;

      if (caption != '') {
        html += `<figcaption class="caption">${caption}</figcaption>`;
      }
      html += '</figure>';

      $(this).wrap(html);

      const mapConfig = {
        tileLayer: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        attribution: 'Map data &copy; <a href="http://www.osm.org">OpenStreetMap</a>',
        markerLng: lng,
        markerLat: lat,
        zoomLevel: zoom
      };

      let map = L.map(`map${index}`, {
        center: [mapConfig.markerLat, mapConfig.markerLng],
        zoom: mapConfig.zoomLevel
      });

      L.tileLayer(mapConfig.tileLayer, {
        attribution: mapConfig.attribution
      }).addTo(map);

      L.marker([mapConfig.markerLat, mapConfig.markerLng], { icon: markerIcon }).addTo(map);

      $(this).remove();
    });
  };

  const getParameterByName = (name: string, querystring: string): string => {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    let regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
      results = regex.exec(querystring);
    return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };

  return {
    init,
    getParameterByName
  };
});
