define(['jquery'], ($: JQueryStatic): UIKit.Module.Accordion => {
  let totalAccordions: number;

  /**
   * return the current context description
   * @param input pipped separate value
   */
  const extractLanguageSubstring = (input: string): string => {
    const languageCode: string = $('html').attr('lang') || 'en';
    let outputString: string;

    /* Check for bilingual */
    const isMultilingual: boolean = input.indexOf('|') > -1;

    if (isMultilingual === true) {
      let languageStrings: UIKit.SquizDescription = {
        en: '',
        cy: ''
      };

      /* remove trailing pipe characters */
      input = input.replace(/\|$/, '');

      /* split languages */
      const arrLanguages: string[] = input.split('|');

      arrLanguages.forEach((languageItem: string): void => {
        const arrLanguageItem: string[] = languageItem.split('=');
        languageStrings[arrLanguageItem[0]] = arrLanguageItem[1];
      });

      /* return the appropriate string for the current language */
      outputString = languageStrings[languageCode];
    } else {
      // return it, just normal
      outputString = input;
    }

    return outputString;
  };

  /**
   * Translation function
   * @param attribute Translation key
   * @param contextOverride Context to override with
   */
  const i18n = (attribute: string, contextOverride: string | undefined = undefined): string => {
    const context: string = contextOverride || $('html').attr('lang') || 'en';

    const translations: UIKit.Translations = {
      en: {
        openAll: 'Open all',
        closeAll: 'Close all'
      },
      cy: {
        openAll: 'Agor pob un',
        closeAll: 'Cau pob un'
      }
    };

    return translations[context][attribute] || '';
  };

  /**
   * Transform Squiz accordion bodycopy to accordion.
   */
  const create = (): void => {
    totalAccordions = 1;

    if ($('.accordion-body.squiz-bodycopy').length > 0) {
      $('.accordion-body.squiz-bodycopy')
        .filter(function(): boolean {
          return !$(this)
            .prev()
            .is('.accordion-body.squiz-bodycopy');
        })
        .map(function(this: HTMLElement): JQuery<HTMLElement> {
          return $(this)
            .nextUntil(':not(.accordion-body.squiz-bodycopy)')
            .addBack();
        })

      $('.accordion-body.squiz-bodycopy').each(function(this: HTMLElement): void {
        $(this).removeClass('accordion-body squiz-bodycopy in');
        $(this).addClass('accordion-section-body');

        /* Get parent ID */
        let id: string | undefined = $(this).attr('id');
        if (id === undefined) {
          id = Math.random()
            .toString(6)
            .substring(2, 8);
          $(this).attr('id', `accordion-${id}`);
        } else if (!isNaN(parseInt(id.charAt(0)))) {
          $(this).attr('id', `accordion-${id}`);
        }

        /* Rename group to section */
        let section = $(this).parents('.accordion-group');
        section.addClass('accordion-section');
        section.removeClass('accordion-group');

        let descString: string | undefined = $(this).attr('desc');
        if (descString === undefined) {descString = '';}

        $(this).siblings('.accordion-heading').remove();

        const headerTemplate = `<div class="accordion-section-header">
          <a class="accordion-section-header-title collapsed"
            tabindex="0"
            role="button" aria-expanded="false"
            data-toggle="collapse"
            aria-controls="#${this.id}"
            href="#${this.id}">
            <svg class="icon icon-chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.75 32">
              <title>Chevron right</title>
                <path d="M6.13,28.28L18.41,16,6.13,3.72,9.84,0l16,16-16,16Z" transform="translate(-6.13 0)"></path>
            </svg>${extractLanguageSubstring(descString)} 
          </a>
        </div>`

        $(this).before(headerTemplate);
      })
    }

    $('.accordion').each(function(this: HTMLElement): void {
      if ($(this).children('.accordion-section').length > 1 &&
        ($(this).data('collapseAll') === undefined || $(this).data('collapseAll') === 'true')) {
        let parentID: string | undefined = $(this).attr('id');
        if (parentID === undefined) {
          parentID = `accordion-${totalAccordions++}`;
          $(this).attr('id', parentID);
        }

        $(this).prepend(`
          <div class="text-right">
            <button class="btn btn-link pointer" aria-expanded="false" type="button" data-collapse-section="${parentID}">${i18n('openAll')}</button>
          </div>`);
      }
    });
  }

  /**
   * Updates the close all button with the aria label and text
   * @param button The button html object
   * @param status Whether
   */
  const updateExpanded = (button: JQuery<HTMLElement>, status: boolean): void => {
    button.text(i18n(!status ? 'openAll' : 'closeAll'));
    button.attr('aria-expanded', status.toString());
  };

  /**
   * Opens all accordion-sections under parent
   * @param target HTML target of the button
   * @param parentID The ID of the parent
   */
  const openAll = (target: JQuery<HTMLElement>, parentID: string): void => {
    if ($(`#${target.data('collapse-section')} .accordion-section-body.collapsing`).length === 0) {
      $(`#${parentID} .accordion-section-body`).collapse('show');
      updateExpanded(target, true);
    }
  };

  /**
   * Closes all accordion-sections under parent
   * @param target HTML target of the button
   * @param parentID The ID of the parent
   */
  const closeAll = (target: JQuery<HTMLElement>, parentID: string): void => {
    if ($(`#${target.data('collapse-section')} .accordion-section-body.collapsing`).length === 0) {
      $(`#${parentID} .accordion-section-body`).collapse('hide');
      updateExpanded(target, false);
    }
  };

  /**
   * Initalises all accordions.
   * @requires bootstrap/collapse
   */
  const init = (): void => {
    create();

    $(document).on('click', 'button[data-collapse-section]', (evt: JQuery.ClickEvent): void => {
      evt.preventDefault();
      const btn: JQuery<HTMLElement> = $(evt.target);

      const isExpanded: boolean = btn.attr('aria-expanded') === 'true' || false;
      const parentID: string | undefined = btn.data('collapseSection');

      if (parentID !== undefined) {
        if (!isExpanded) {
          openAll(btn, parentID);
        } else {
          closeAll(btn, parentID);
        }
      }
    });

    $(document).on('hidden.bs.collapse', '.accordion-section-body', function(this: HTMLElement): void {
      const accordion: JQuery<HTMLElement> = $(this)
        .parent()
        .parent();
      if (accordion.find('.accordion-section').length > 1) {
        const btn: JQuery<HTMLElement> = accordion.find('.text-right .btn');

        if (accordion.find('.accordion-section .accordion-section-header .accordion-section-header-title:not(.collapsed)').length === 0) {
          updateExpanded(btn, false);
        }
      }
    });

    $(document).on('shown.bs.collapse', '.accordion-section-body', function(this: HTMLElement): void {
      const accordion: JQuery<HTMLElement> = $(this)
        .parent()
        .parent();
      if (accordion.find('.accordion-section').length > 1) {
        const btn: JQuery<HTMLElement> = accordion.find('.text-right .btn');

        if (accordion.find('.accordion-section .accordion-section-header .accordion-section-header-title.collapsed').length === 0) {
          updateExpanded(btn, true);
        }
      }
    });
  };

  return {
    init,
    i18n,
    openAll,
    closeAll,
    extractLanguageSubstring,
    create,
    updateExpanded
  };
});
