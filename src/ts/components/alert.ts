define(['jquery'], ($: JQueryStatic): UIKit.Module.Alert => {
  /**
   * return the current context description
   * @param input pipped separate value
   */
  const extractLanguageSubstring = (input: string): string => {
    const languageCode: string = $('html').attr('lang') || 'en';
    let outputString: string;

    /* Check for bilingual */
    const isMultilingual: boolean = input.indexOf('|') > -1;

    if (isMultilingual === true) {
      let languageStrings: UIKit.SquizDescription = {
        en: '',
        cy: ''
      };

      /* remove trailing pipe characters */
      input = input.replace(/\|$/, '');

      /* split languages */
      const arrLanguages: string[] = input.split('|');

      arrLanguages.forEach((languageItem: string): void => {
        const arrLanguageItem: string[] = languageItem.split('=');
        languageStrings[arrLanguageItem[0]] = arrLanguageItem[1];
      });

      /* return the appropriate string for the current language */
      outputString = languageStrings[languageCode];
    } else {
      // return it, just normal
      outputString = input;
    }

    return outputString;
  };

  /**
   * Adds the alert class and the title if present
   */
  const create = (): void => {
    if ($('.alert-body.squiz-bodycopy').length) {
      $('.alert-body.squiz-bodycopy').each(function(): void {
        $(this).addClass('alert');
        $(this).addClass('alert-block');

        let descString: string | undefined = $(this).attr('desc');
        if (descString !== undefined) {
          $(this).prepend(`<h4 class="alert-title">${extractLanguageSubstring(descString)}</h4>`);
        }
      });
    }
  };

  /**
   * The initialiser for clearing
   */
  const init = (): void => {
    create();
  };

  return {
    init,
    create,
    extractLanguageSubstring
  };
});
