define([], (): object => {
  requirejs.config({
    paths: {
      jquery: '../lib/jquery-3.4.1',
      prettier: '../lib/prettier',
      popper: '../lib/popper',
      bootstrap: '../lib/bootstrap/bootstrap',
      promise: '../lib/polyfill/promise',
      mapboxgl: '../lib/mapbox/mapbox-gl',
      'mapboxgl-supported': '../lib/mapbox/mapbox-gl-supported',
      leaflet: '../lib/leaflet/leaflet',
      turf: '../lib/mapbox/turf',
      observer: '../lib/polyfill/observer',
      tableSort: '../lib/table/table-sort',
      embedly: '../lib/embedly/embedly',
      sticky: '../lib/polyfill/sticky',
      validate: '../lib/validate/validate',
      mathJax: '../lib/mathJax',
      slick: '../lib/slick/js/slick',
      sortable: '../lib/jquery-ui/jquery-ui-drag-n-drop-sortable',
      touchPunch: '../lib/jquery-ui/jquery-ui-touch-punch',
      svgxuse: '../lib/polyfill/svgxuse',
      hoverIntent: '../lib/hover-intent/hover-intent.min',
    },
    shim: {
      tableSort: {
        deps: ['jquery'],
      },
      popper: {
        exports: 'popper.js',
      },
      bootstrap: {
        deps: ['popper', 'jquery'],
        exports: 'bootstrap',
      },
      validate: {
        deps: ['jquery'],
        exports: 'validate',
      },
      'components/map': {
        deps: ['leaflet'],
      },
    },
  });

  const version = '{{{UIKitVersion}}}';
  const cdn = 'https://d1fccdceamnhxg.cloudfront.net/';
  const local = 'http://localhost:8080/';
  const env: string = document.location.href.indexOf('localhost:8080') !== -1 ? 'dev' : 'prod';

  const loadCSS = function (url: string): void {
    let link = document.createElement('link');
    link.type = 'text/css';
    link.rel = 'stylesheet';
    link.href = env === 'prod' ? `${cdn}${version}/${url}` : `${local}dist/${url}`;
    document.getElementsByTagName('head')[0].appendChild(link);
  };

  /* Check for promises */
  if (typeof Promise === 'undefined') {
    requirejs(['promise'], (p: any): void => {
      // eslint-disable-next-line
      var Promise = p;
    });
  }

  const globals = (callback: Require): void => {
    callback(
      ['bootstrap', 'jquery', 'components/nav-global.min', 'components/search-form.min', 'components/nav-search.min'],
      (bs: JQuery, $: JQueryStatic, navGlobal: any, searchForm: UIKit.Module.SearchForm, navSearch: UIKit.Module.NavSearch): void => {
        new navGlobal.Navigation();
        navSearch.init();
        searchForm.init();

        const body = document.querySelector('body');
        if (body !== null) {
          body.classList.add('js-loaded');
        }
      }
    );
  };

  const checkCarousel = (callback: Require): void => {
    if (document.querySelector('.carousel') !== null) {
      callback(['bootstrap', 'components/carousel.min'], (bs: Function, carousel: UIKit.Module.Carousel): void => {
        carousel.init();
      });
    }
  };

  const checkLazyLoad = (callback: Require): void => {
    if (document.querySelector('img.lazy') !== null) {
      if (typeof IntersectionObserver === 'undefined') {
        requirejs(['observer', 'components/lazy-loader.min'], (intersect: any, lazy: UIKit.Module.LazyLoader): void => {
          // eslint-disable-next-line
          var IntersectionObserver = intersect;
          lazy.init();
        });
      } else {
        callback(['components/lazy-loader.min'], (lazy: UIKit.Module.LazyLoader): void => {
          lazy.init();
        });
      }
    }
  };

  const checkTooltip = (callback: Require): void => {
    if (document.querySelector('[data-toggle="tooltip"]') !== null) {
      callback(['bootstrap'], (): void => {
        $('[data-toggle="tooltip"]').tooltip();
      });
    }
  };

  const checkPills = (callback: Require): void => {
    if (document.querySelector('.nav-pills') !== null) {
      callback(['components/pills.min'], (pills: UIKit.Module.Pills): void => {
        pills.init();
      });
    }
  };

  const checkSticky = (callback: Require): void => {
    if (document.querySelector('.sticky-top, .sticky') !== null) {
      const el = document.createElement('a');
      const mStyle = el.style;
      let supported = true;
      mStyle.cssText = 'position:sticky;position:-webkit-sticky;position:-ms-sticky;';

      if (mStyle !== null && mStyle.position !== null) {
        supported = mStyle.position.indexOf('sticky') !== -1;
      } else {
        supported = false;
      }

      if (!supported) {
        callback(['sticky'], (stickybits: any): void => {
          stickybits('.sticky-top, .sticky');
        });
      }
    }
  };

  const checkSubmitForm = (callback: Require): void => {
    if (document.querySelector('.form input[type="submit"]') !== null) {
      callback(['components/submit-form.min'], (submitform: UIKit.Module.SubmitForm): void => {
        submitform.init();
      });
    }
  };

  const checkMap = (callback: Require): void => {
    if (document.querySelector('.map-link') !== null) {
      callback(['components/map.min'], (map: UIKit.Module.Map): void => {
        loadCSS('lib/leaflet/leaflet.css');
        map.init();
      });
    }
  };

  const checkAccordions = (callback: Require): void => {
    if (document.querySelector('.accordion') !== null || document.querySelector('.accordion-body.squiz-bodycopy') !== null) {
      callback(['bootstrap', 'components/accordion.min'], (bs: Function, accordion: UIKit.Module.Accordion): void => {
        accordion.init();
      });
    }
  };

  const checkTabFocus = (callback: Require): void => {
    if (window.location.href.indexOf('#') > -1) {
      callback(['bootstrap', 'components/tab-focus.min'], (_: any, tabs: UIKit.Module.TabFocus): void => {
        tabs.init();
      });
    }
  };

  const checkTables = (callback: Require): void => {
    if (document.querySelector('table') !== null) {
      if (document.querySelector('.table-sortable') !== null) {
        callback(['jquery', 'tableSort', 'components/table.min'], ($: any, _: any, table: UIKit.Module.Table): void => {
          table.init();
        });
      } else {
        callback(['jquery', 'components/table.min'], (_: any, table: UIKit.Module.Table): void => {
          table.init();
        });
      }
    }
  };

  const checkEmbed = (callback: Require): void => {
    if (document.querySelector('.embed') !== null) {
      callback(['embedly', 'components/embed.min'], (embedly: any, embed: UIKit.Module.Embed): void => {
        embed.init();
      });
    }
  };

  const checkCircleNavigation = (callback: Require): void => {
    if (document.querySelector('ol.circle-navigation') !== null && window.matchMedia('(min-width: 980px)').matches) {
      callback(['components/circle-navigation.min'], (circleNavigation: UIKit.Module.CircleNavigation): void => {
        circleNavigation.init();
      });
    }
  };

  const checkWPFeed = (callback: Require): void => {
    if (document.querySelector('.wpfeed-link') !== null) {
      callback(['components/wpfeed.min'], (wpfeed: UIKit.Module.WPFeed): void => {
        wpfeed.init();
      });
    }
  };

  const checkProcess = (callback: Require): void => {
    if (document.querySelector('.process') !== null) {
      callback(['components/process.min'], (process: UIKit.Module.Process): void => {
        process.init();
      });
    }
  };

  const checkAppsToolbar = (callback: Require): void => {
    if (document.querySelector('.apps-toolbar') !== null) {
      // eslint-disable-next-line
      callback(
        ['jquery', 'svgxuse', 'sortable', 'components/apps-toolbar.min'],
        ($: JQueryStatic, svgxuse: any, sortable: any, appsToolbar: UIKit.Module.AppsToolbar): void => {
          loadCSS('lib/slick/css/slick-theme.css');
          loadCSS('lib/slick/css/slick.css');
          appsToolbar.init();
        }
      );
    }
  };

  const checkHandbookSearch = (callback: Require): void => {
    if (document.querySelector('.container-handbooksearch') !== null) {
      callback(['components/handbook.min'], (handbook: UIKit.Module.Handbook): void => {
        handbook.init();
      });
    }
  };

  const checkModulesearch = (callback: Require): void => {
    if (document.querySelector('.container-modulesearch') !== null) {
      callback(['components/module-search.min'], (moduleSearch: UIKit.Module.ModuleSearch): void => {
        moduleSearch.init();
      });
    }
  };

  const checkFacet = (callback: Require): void => {
    if (document.querySelector('.facet') !== null) {
      callback(['components/facet.min'], (facet: UIKit.Module.Facet): void => {
        facet.init();
      });
    }
  };

  const checkTabs = (callback: Require): void => {
    if (document.querySelector('.tab-pane.squiz-bodycopy') !== null) {
      callback(['bootstrap', 'components/tabs.min'], (_: any, tabs: UIKit.Module.Tab): void => {
        tabs.init();
      });
    }
  };

  const checkYammer = (callback: Require): void => {
    if (document.querySelector('#yammer-login') !== null) {
      callback(['components/yammer-embed.min'], (yammer: UIKit.Module.YammerEmbed): void => {
        yammer.init();
      });
    }
  };

  const checkAlert = (callback: Require): void => {
    if (document.querySelector('.alert') !== null || document.querySelector('.alert-body.squiz-bodycopy') !== null) {
      callback(['bootstrap', 'components/alert.min'], (bs: JQuery, alert: UIKit.Module.Alert): void => {
        alert.init();
      });
    }
  };

  const init = (): void => {
    globals(requirejs);
    checkAppsToolbar(requirejs);
    checkCarousel(requirejs);
    checkLazyLoad(requirejs);
    checkTooltip(requirejs);
    checkPills(requirejs);
    checkSticky(requirejs);
    checkSubmitForm(requirejs);
    checkMap(requirejs);
    checkAccordions(requirejs);
    checkTabFocus(requirejs);
    checkTables(requirejs);
    checkEmbed(requirejs);
    checkYammer(requirejs);
    checkCircleNavigation(requirejs);
    checkWPFeed(requirejs);
    checkProcess(requirejs);
    checkHandbookSearch(requirejs);
    checkModulesearch(requirejs);
    checkFacet(requirejs);
    checkTabs(requirejs);
    checkAlert(requirejs);
  };

  init();

  return {
    init,
    checkLazyLoad,
    checkTooltip,
    checkPills,
  };
});
