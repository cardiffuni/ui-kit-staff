import { Move } from './move.mjs';
import fs from 'fs';

const pack = JSON.parse(fs.readFileSync('./package.json').toString());

Move('./dist/css', `./dist/${pack.UIKitVersion}/css/`).catch((e) => {
  throw Error('Move failed:', e);
});

Move('./dist/js', `./dist/${pack.UIKitVersion}/js/`).catch((e) => {
  throw Error('Move failed:', e);
});

Move('./dist/webfonts', `./dist/${pack.UIKitVersion}/webfonts/`).catch((e) => {
  throw Error('Move failed:', e);
});

Move('./dist/ico', `./dist/${pack.UIKitVersion}/ico/`).catch((e) => {
  throw Error('Move failed:', e);
});

Move('./dist/lib', `./dist/${pack.UIKitVersion}/lib/`).catch((e) => {
  throw Error('Move failed:', e);
});
