import fs from 'fs'
import rimraf from 'rimraf'
import fsExtra from 'fs-extra'

const paths = {
  components: './src/html/components/',
  layouts: './src/html/layouts/',
  pages: './src/html/pages/',
  tests: './src/html/tests/',
  svg: './src/svg/',
  docs: './docs/',
  output: {
    pages: './docs/pages/',
    components: './docs/components/',
    layouts: './docs/layouts/'
  },
  jest: './tests/unit/html/'
}

let globalPageGenerated = [];

const blacklist = [
  'head',
  'global-footer',
  'global-header',
  '.gitkeep'
]

const regexs = {
  includes: /\$\{require\(((\'|\")([a-zA-Z-0-9\/.]+)(\'|\"))\)\}/g,
  svgs: /\$\{require-svg\(((\'|\")([a-zA-Z-0-9\/.]+)(\'|\"))\)\}/g,
  filePath: /(\'|\")([a-zA-Z-0-9\/.]+)(\'|\")/,
  quotes: /(\'|\")/g
}

/**
 * Resets test and docs html
 */
const reset = () => {
  rimraf.sync(`${paths.docs}components`)
  rimraf.sync(`${paths.docs}layouts`)
  rimraf.sync(`${paths.docs}pages`)
  rimraf.sync(paths.jest)
}

/**
 * Converts a file name to title case
 * @param {string} str Raw string to convert
 * @author Christopher Atwood
 * @since 1.0.0
 */
const titleCase = str =>
  str
    .replace('.html', '')
    .toLowerCase()
    .split('-')
    .map(word => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');


/**
 * Recursively builds up a HTML page by including all SVG and html partials
 * @param {String} path path to the file
 * @param {string} component component name
 */
const htmlBuilder = (path, component) => {
  let html = fs.readFileSync(path).toString()

  /* Check for any imports */
  const svgsResults = html.match(regexs.svgs)
  const includesResults = html.match(regexs.includes)

  if (includesResults) {
    const splits = path.split('/')
    component = splits[splits.length - 2]
    component = (component === "pages" || component === "tests" || component === "layouts" )? "" : component

    includesResults.forEach((value, index) => {
      component = component.includes('.html') ? '' : `${component.replace(/\//, '')}/`

      const path = `${paths.components}${component}${value.match(regexs.filePath)[0].toString().replace(regexs.quotes, '')}`
      const ext = (path.includes('.html')) ? '': '.html'
      const partial = fs.readFileSync(`${path}${ext}`).toString()

      if (partial.match(regexs.includes) || partial.match(regexs.svgs)) {
        html = html.replace(value, htmlBuilder(path, component))
      } else {
        html = html.replace(value, partial)
      }
    })
  }

  if (svgsResults) {
    svgsResults.forEach((value, index) => {
      const path = `${paths.svg}${value.match(regexs.filePath)[0].toString().replace(regexs.quotes, '')}`
      const ext = (path.includes('.svg')) ? '': '.svg'
      const partial = fs.readFileSync(`${path}${ext}`).toString()

      html = html.replace(value, partial)
    })
  }
  return html
}


/**
 * Builds up the final HTML page
 * @param {String} componentPath Component path to build recursively
 * @param {String} component component name
 * @param {Boolean} wrapper Whether to include a wrapper around HTML
 */
const template = (componentPath, component, wrapper) => {
  let document = htmlBuilder(`${paths.components}head/head.html`, 'head');
  document += htmlBuilder(`${paths.components}header-global/header-global.html`, 'header-global');
  if (wrapper) {
    document += '<div class="content" id="content"><div class="container"><div class="row"><div class="col-md-12">'
  }

  document += htmlBuilder(`${componentPath}`, component)

  if (wrapper) {
    document += '</div></div></div></div>'
  }

  document += htmlBuilder(`${paths.components}footer-global/footer-global.html`, 'footer-global')

  return document
}


/**
 * Stage all files to build HTML
 * @param {*} path Root path
 * @param {*} outPath Output file to generate HTML
 * @param {boolean} wrapper Whether to include a wrapper
 * @param {boolean} generateIndexSections Whether it's only generating the index page.
 * @author Christopher Atwood
 */
const checkDirectory = (path, outPath = paths.docs, wrapper = false, generateIndexSections = true) => {
  fs.readdirSync(path).forEach(component => {
    const valid = fs.statSync(`${path}${component}`).isDirectory();

    if (blacklist.indexOf(component) === -1) {
      if (valid) {
        fs.readdirSync(`${path}${component}`).forEach(file => {
          const isFile = fs.statSync(`${path}${component}/${file}`).isFile();

          if (isFile) {
            if (generateIndexSections) {
              /* add into section nav */
              globalPageGenerated.push({
                html: htmlBuilder(`${path}${component}/${file}`, component),
                page: {
                  name: titleCase(file),
                  url: `${file}`
                }
              });
            } else {
              fsExtra.outputFileSync(`${outPath}${file}`, template(`${path}${component}/${file}`, component, wrapper));
            }
          }
        });
      } else {
        if (outPath === paths.jest) {
          const document = htmlBuilder(`${path}${component}`, component);

          fsExtra.outputFileSync(`${outPath}${component}`, document);
        } else {
          if (generateIndexSections) {
            /* add into section nav */
            globalPageGenerated.push({
              html: htmlBuilder(`${path}${component}`, component),
              page: {
                name: titleCase(component),
                url: `${component}`
              }
            });
          }

          if (!generateIndexSections) {
            fsExtra.outputFileSync(`${outPath}${component}`, template(`${path}${component}`, component, wrapper));
          }
        }
      }
    }
  });

  if (generateIndexSections === true) {
    checkDirectory(path, outPath, wrapper, false);
  }
};

/**
 * Builds up the section/ sticky navigation for the section page.
 * 
 * @param {string} currentPage The name of the current page
 * @param {boolean} isSticky If the navigation should be sticky (used for index.html)
 * @param {boolean} useID Whether the href should be an id or url path
 * @since 1.0.0
 * @author Christopher Atwood
 */
const buildSectionNav = (currentPage, isSticky = false, useID = false) => {
  let sectionNav = `
  <div class="nav-section ${isSticky ? 'sticky-top mb-40' : ''}" style="">
    ${isSticky ? '<div class="container">' : ''}
    <div class="row">
      <div class="col-md-12">
        <nav class="navbar">
          <ul class="nav">
            {{{items}}}
            <li class="dropdown dropdown-more nav-item d-none invisible" aria-hidden="true">
              <a href="#" class="nav-link dropdown-toggle">More</a>
              <ul class="dropdown-menu dropdown-menu-more dropdown-menu-right"></ul>
            </li>
          </ul>
        </nav>
      </div>
    </div>
    ${isSticky ? '</div>' : ''}
  </div>`;

  let items = '';

  globalPageGenerated.forEach(component => {
    items += `<li class="nav-item ${currentPage === component.page.name ? 'active' : ''}"><a class="nav-link" href="${
      useID ? `#${component.page.name.toLowerCase().replace('.html', '').replace(/ /g, '-')}` : component.page.url
    }">${component.page.name}</a></li>`;
  });

  return sectionNav.replace('{{{items}}}', items);
};

/**
 * Builds the index.html page for every component in the UI kit
 * @since 1.0.0
 * @author Christopher Atwood
 */
const buildComponentsIndex = () => {
  const indexHTMLContent = `
    ${htmlBuilder(`${paths.components}head/head.html`, 'head')}
    ${htmlBuilder(`${paths.components}header-global/header-global.html`, 'header-global')}
    <div class="content" id="content">
      <div class="container">
        <div class="row">
          <div class="col-md-10"><h1 class="colour-red">All components</h1></div>
          <div class="col-md-12">
            {{{items}}}
          </div>
        </div>
      </div>
    </div>
    ${htmlBuilder(`${paths.components}footer-global/footer-global.html`, 'footer-global')}
  `;

  const componentHTML = `<div id="{{{id}}}"></div>{{{componentHTML}}}`;

  let items = '';

  globalPageGenerated.forEach(component => {
    items += componentHTML.replace('{{{id}}}', component.page.name.replace(/ /g, '-').toLowerCase()).replace('{{{componentHTML}}}', component.html);
  });

  fsExtra.outputFileSync(`${paths.output.components}index.html`, indexHTMLContent.replace('{{{items}}}', items))
};

reset()

checkDirectory(paths.components, paths.output.components, true, true);
checkDirectory(paths.pages, paths.output.pages, false, false);
checkDirectory(paths.layouts, paths.output.layouts, false, false);
checkDirectory(paths.tests, paths.jest, false, false);
buildComponentsIndex()