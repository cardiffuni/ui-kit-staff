import fs from 'fs';
import fsExtra from 'fs-extra';

/**
 * Adds a version to typescript
 */
const addVersion = () => {
  const pack = JSON.parse(fs.readFileSync('./package.json').toString());

  let js = fs.readFileSync('dist/js/main.js').toString();
  let jsMin = fs.readFileSync('dist/js/main.min.js').toString();
  js = js.replace('{{{UIKitVersion}}}', pack.UIKitVersion);
  jsMin = jsMin.replace('{{{UIKitVersion}}}', pack.UIKitVersion);

  fsExtra.outputFileSync(`dist/js/main.js`, js);
  fsExtra.outputFileSync(`dist/js/main.min.js`, jsMin);
};

addVersion();
