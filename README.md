[![Run Status](https://api.shippable.com/projects/5d1206adcecb900006e49905/badge?branch=master)]() [![Coverage Badge](https://api.shippable.com/projects/5d1206adcecb900006e49905/coverageBadge?branch=master)]()

# Table of contents

- [Table of contents](#table-of-contents)
  - [Globals NPM installs required](#globals-npm-installs-required)
    - [Eslint](#eslint)
    - [Browser sync](#browser-sync)
    - [Deployment](#deployment)
      - [Install python](#install-python)
      - [Install AWS cli](#install-aws-cli)
      - [Set your enviroment variables](#set-your-enviroment-variables)
      - [Configure AWS](#configure-aws)
  - [Styles removed](#styles-removed)
  - [Docs generation](#docs-generation)
    - [HTML Partials](#html-partials)
    - [SVG partials](#svg-partials)
    - [Component](#component)
    - [Pages and layouts](#pages-and-layouts)
  - [Tests](#tests)

## Globals NPM installs required

### Eslint

```bash
npm i -g eslint
```

### Browser sync

This is used for serving static content in a development area.

```bash
npm i -g browser-sync
```

### Deployment

#### Install python

[Install here](https://www.python.org/downloads/)

Download the latest version of pythyon. Currently **3.7.3**.

Administration permission is not requried.

#### Install AWS cli

```bash
pip3 install awscli --upgrade --user
```

[Help](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)

#### Set your enviroment variables

- [Windows](https://docs.aws.amazon.com/cli/latest/userguide/install-windows.html#awscli-install-windows-path)
- [macOS](https://docs.aws.amazon.com/cli/latest/userguide/install-macos.html#awscli-install-osx-path)
- [Linux](https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html#install-linux-path)

#### Configure AWS

Request AWS configurations by emailing [web@cardiff.ac.uk](mailto:web@cardiff.ac.uk)

## Styles removed

| File           | Style                   | Explination         |
| -------------- | ----------------------- | ------------------- |
| type.sass      | h7 tag removed.         | No a valid selector |


## Docs generation

All pages within the ``./src/html/components/**/*.html`` will automatically generate a HTML page with correct head and footer  elements. These pages will generate a basic HTML page with ``.content>.container>.row>.col-md-12``.  If you require a different layout for the component, then you can create a page within pages.

### HTML Partials

``` Javascript
  ${require('variations/accordion-squiz.html')}

  // OR
  ${require('variations/accordion-squiz')}
```

### SVG partials

``` Javascript
  ${require-svg('chevron-up.svg')}

  // OR
  ${require-svg('chevron-up')}
```

### Component

### Pages and layouts

All pages will get generated with the head and footer elements, thus, **do not include imports for those components in your page**.

You can add whatever markup you like in the page, there is no wrapping container unlike components file.

## Tests

Test pages do not generate with head or footer components.You must include all partials you which to test.
