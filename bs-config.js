module.exports = {
  cors: true,
  noopen: true,
  notify: false,
  port: 8080,
  files: ['./dist', './src/html'],
  server: {
    baseDir: "./",
    directory: true
  },
};